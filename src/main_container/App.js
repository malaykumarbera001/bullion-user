import React, { Component } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import './App.css';
import { checkAuth } from '../utils/auth'
import {
  CSSTransition,
  TransitionGroup,
} from 'react-transition-group';
import { Alert } from 'reactstrap';
import MainHome from '../components/home_components/MainHome';
import PopupMessage from '../components/home_components/PopupMessage';

import Header from '../components/layout_components/Header1';
import Footer from '../components/layout_components/Footer';
import Buy from '../components/buy_components/Buy';
import BankDetails from '../components/other_components/BankDetails';
import Message from '../components/other_components/Message';
import About from '../components/other_components/About';
import Policy from '../components/other_components/Policy';
import Contact from '../components/other_components/Contact';
import Trade from '../components/profille_components/Trade';
import TradeHistory from '../components/profille_components/TradeHistory';
import PendingOrder from '../components/profille_components/PendingOrder';
import ModifyPendingOrder from '../components/buy_components/ModifyPendingOrder';
import Login from '../components/auth_components/Login';
import Register from '../components/auth_components/Register';
import KYCDetails from '../components/profille_components/KYCDetails';
import MyAccount from '../components/profille_components/MyAccount';
import { ToastContainer, toast } from 'react-toastify';
import 'react-responsive-modal/styles.css';
import OpenCloseReport from '../components/graph_report_components/OpenCloseReport';
import Charts from '../components/graph_report_components/Charts';
import Charts2 from '../components/graph_report_components/Charts2';
import Charts1 from '../components/graph_report_components/Charts1';
import Charts_test from '../components/graph_report_components/Charts_test';
export default class App extends Component {

  componentDidMount() {
    document.addEventListener("contextmenu", this.handleContextMenu);

  }

  componentWillUnmount() {
    document.removeEventListener("contextmenu", this.handleContextMenu);


  }

  handleContextMenu = (e) => {
    e.preventDefault()
    console.log(e)
  }


  render() {
    return (
      <div>
        <SkeletonTheme color="#192133" highlightColor="#1f273b">
          <Header {...this.props} />
          <PopupMessage />
          <Route render={({ location }) => (
            <TransitionGroup >
              <CSSTransition
                key={location.key}
                timeout={0}
                classNames="fade"
              >
                <div className="divHight" style={{ overflowX: "hidden" }}>
                  <Switch location={location}>
                    <Route exact path='/' component={MainHome}></Route>
                    <Route path='/buy' component={Buy}></Route>
                    <Route path='/bankDetails' component={BankDetails}></Route>
                    <Route path='/message' component={Message}></Route>
                    <Route path='/about' component={About}></Route>
                    <Route path='/policy' component={Policy}></Route>
                    <Route path='/contact' component={Contact}></Route>
                    <Route path='/login' component={Login}></Route>
                    <Route path='/register' component={Register}></Route>
                    <Route path='/openCloseReport' component={OpenCloseReport}></Route>
                    <Route path='/candleStickCharts' component={Charts}></Route>
                    <Route path='/currentCharts' component={Charts1}></Route>
                    <Route path='/highLowCharts' component={Charts2}></Route>
                    <Route path='/testCandle' component={Charts_test}></Route>







                    <PrivateRoute path='/trade' component={Trade}></PrivateRoute>
                    <PrivateRoute path='/tradeHistory' component={TradeHistory}></PrivateRoute>
                    <PrivateRoute path='/pendingOrder' component={PendingOrder}></PrivateRoute>
                    <PrivateRoute path='/modifyOrder' component={ModifyPendingOrder}></PrivateRoute>
                    <PrivateRoute path='/kycDetails' component={KYCDetails}></PrivateRoute>
                    <PrivateRoute path='/account' component={MyAccount}></PrivateRoute>














                  </Switch>
                </div>
              </CSSTransition>
            </TransitionGroup>
          )} />
          <Footer />

        </SkeletonTheme>
      </div>
    )
  }
}

const PrivateRoute = ({ component: Component, ...rest }) => (

  <Route {...rest} render={(props) => (
    checkAuth.isAuthenticated
      ? <Component {...props} />
      : <Redirect to='/login' />
  )} />
)