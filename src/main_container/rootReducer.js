import { combineReducers } from "redux";
import reducer from "../components/auth_components/reducer";

const rootReducer = combineReducers({
  profile: reducer,

});


export default rootReducer;
