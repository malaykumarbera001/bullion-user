import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { authpost, get } from '../../utils/service'
import Moment from 'moment-js';
import { ToastContainer, toast } from 'react-toastify';

var urlCrypt = require('url-crypt')('~{ry*I)==yU/]9<7DPk!Hj"R#:-/Z7(hTBnlRS=4CXF');

export default class PendingOrder extends Component {
    constructor(props) {
        super(props)

        this.state = {
            marquee: [],
            trade: []
        }
    }
    getMarquee = () => {
        get('/user/notification/')
            .then((res) => {
                console.log(res)
                this.setState({
                    marquee: res.data.filter(word => word.page == 'Pending Order')
                })
            }).catch((err) => {
                console.log(err)
            })
    }
    getTrade = () => {
        authpost('/order/futureOrder/')
            .then((res) => {
                console.log(res)
                this.setState({
                    trade: res.data
                })
            }).catch((err) => {
                console.log(err)
            })
    }
    componentDidMount() {
        document.title = 'Pending Order | Pawar Bullion'
        window.scrollTo(0, 0)
        this.getMarquee()
        this.getTrade()
    }
    handleCancel = (id) => {
        const data = {
            'id': id
        }
        authpost('/order/cancelFutureOrder/', data)
            .then((res) => {
                if (res.data.status) {
                    this.getTrade()
                    toast.dark(res.data.msg, {
                        position: "bottom-center",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                } else {
                    toast.error(res.data.msg, {
                        position: "bottom-center",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }
            }).catch((err) => {
                console.log(err)
            })
    }
    render() {
        return (
            <main id="content" role="main">
                <div class="bg-gray-13 bg-md-transparent">
                    <div class="container">
                        <div class="my-md-3">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><Link to="/">Home</Link></li>
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Pending Order</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="mb-8 text-center">
                        <h1>Pending Order</h1>
                    </div>
                    <div class="table-responsive">
                        <div class="mb-10 cart-table">
                            <form class="mb-4" action="#" method="post">
                                <table class="table" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th class="product-thumbnail">Date</th>
                                            <th class="product-name">Symbol</th>
                                            <th class="product-quantity w-lg-15">Quantity</th>
                                            <th class="product-price">Target Price</th>
                                            <th class="product-price">Total Price</th>
                                            <th class="product-subtotal  min-width-200-md-lg">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        {
                                            this.state.trade.length != 0 ?
                                                this.state.trade.map((obj, i) =>
                                                    <tr class="buttomBorder">

                                                        <td class="d-none d-md-table-cell">
                                                            <span class="">{Moment(obj.date).format('f')}</span>

                                                        </td>
                                                        <td data-title="Order ID">
                                                            <span class="">{obj.orderID}</span>

                                                        </td>

                                                        <td data-title="Symbol">
                                                            <span class="text-gray">{obj.indexTitle}</span>
                                                        </td>



                                                        <td data-title="Quantity">
                                                            <span class="">{obj.indexQuantity}{obj.quantityType}</span>

                                                        </td>
                                                        <td data-title="Price">
                                                            <span class="">      &#8377;{obj.rate}<span style={{ fontSize: "13px" }}>/10gm</span></span>
                                                        </td>

                                                        <td class="nasaans">
                                                            {
                                                                obj.status == 0 ?
                                                                    <>
                                                                        <Link to={"/modifyOrder?NgTh=" + urlCrypt.cryptObj(String(obj.id))}><button type="button" class="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-4 px-lg-5 w-100 w-md-auto">Modify</button></Link>
                                                                        <button type="button" class="btn btn-soft-danger mb-3 mb-md-0 font-weight-normal px-5 px-md-4 px-lg-5 w-100 w-md-auto" onClick={() => this.handleCancel(obj.id)}>Cancel</button>
                                                                    </> :
                                                                    obj.status == 1 ?
                                                                        <>Ordered</> : <span style={{ color: "red", fontWeight: "600" }}>Canceled</span>

                                                            }
                                                        </td>
                                                    </tr>

                                                )
                                                : <tr><td colSpan="5"><center>No Pending Order Found</center></td></tr>
                                        }
                                    </tbody>
                                </table>
                            </form>
                        </div>

                    </div>
                    <div class="row marqueeDiv">
                        <div class="col-md-12 mb-4 mb-md-0">
                            <marquee scrollamount="20" class="marqueeDivMar">

                                {
                                    this.state.marquee.map((obj, i) =>
                                        <>

                                            <div class="customH1 basvamn bgtyd">
                                                {obj.message}
                                            </div>
                                        </>
                                    )
                                }

                            </marquee>
                        </div>
                    </div>


                </div>
                <ToastContainer />

            </main>
        )
    }
}
