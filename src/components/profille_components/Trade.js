import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { authpost, get } from '../../utils/service'
import Moment from 'moment-js';
import { Modal } from 'react-responsive-modal';
import { BsInfoCircle } from "react-icons/bs";
export default class Trade extends Component {
    constructor(props) {
        super(props)

        this.state = {
            marquee: [],
            trade: [],
            modalStatus: false,
            viewDetailsDeliveryArr: [],
            viewDetailsArr: []
        }
    }

    componentDidMount() {
        document.title = 'Trade | Pawar Bullion'
        window.scrollTo(0, 0)
        this.getMarquee()
        this.getTrade()


    }
    onCloseModal = () => {
        this.setState({
            modalStatus: false,

            viewDetailsArr: [],
            viewDetailsDeliveryArr: []
        })
    }
    getMarquee = () => {
        get('/user/notification/')
            .then((res) => {
                console.log(res)
                this.setState({
                    marquee: res.data.filter(word => word.page == 'Trade')
                })
            }).catch((err) => {
                console.log(err)
            })
    }
    getTrade = () => {
        authpost('/order/trades/')
            .then((res) => {
                console.log(res)
                this.setState({
                    trade: res.data
                })
            }).catch((err) => {
                console.log(err)
            })
    }
    viewDetails = (obj) => {
        this.setState({
            viewDetailsArr: obj,
            viewDetailsDeliveryArr: obj.deliveryDetails,
            modalStatus: true
        })
    }
    render() {
        return (
            <main id="content" role="main">
                <div class="bg-gray-13 bg-md-transparent">
                    <div class="container">
                        <div class="my-md-3">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><Link to="/">Home</Link></li>
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Trade</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="mb-8 text-center">
                        <h1>Trade</h1>
                    </div>
                    <div class="table-responsive">
                        <div class="mb-10 cart-table">
                            <form class="mb-4" action="#" method="post">
                                <table class="table" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th class="product-thumbnail">Date</th>
                                            <th class="product-thumbnail">Order ID</th>

                                            <th class="product-name">Symbol</th>
                                            <th class="product-quantity w-lg-15">Quantity</th>
                                            <th class="product-price">Live Rate</th>
                                            <th class="product-price">Payable Price</th>

                                            <th class="product-subtotal">Status</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.trade.length != 0 ?
                                                this.state.trade.map((obj, i) =>

                                                    <tr class="buttomBorder">

                                                        <td data-title="Date">
                                                            <span class="">{Moment(obj.date).format('f')}</span>

                                                        </td>
                                                        <td data-title="Order ID">
                                                            <span class="">{obj.orderID}</span>

                                                        </td>

                                                        <td data-title="Symbol">
                                                            <span class="text-gray">{obj.indexTitle}</span>
                                                        </td>



                                                        <td data-title="Quantity">
                                                            <span class="">{obj.indexQuantity}{obj.quantityType}</span>

                                                        </td>
                                                        <td data-title="Price">
                                                            &#8377;{obj.liveRate}<span style={{ fontSize: "13px" }}>/10gm</span>

                                                        </td>
                                                        <td data-title="Total Price">
                                                            &#8377;{obj.quantityType == 'KG' ? Math.round(((obj.liveRate - obj.discount) / 10) * (1000 * obj.indexQuantity)) : Math.round(((obj.liveRate - obj.discount) / 10) * obj.indexQuantity)}</td>

                                                        <td data-title="Status">
                                                            {obj.status == 0 ?
                                                                <>Order Confirmed</>

                                                                : obj.status == 1 ?
                                                                    <b>Partially Delivered of {obj.deliveredQuantity}{obj.deliveredQuantityType}</b>
                                                                    : obj.status == 2 ?
                                                                        <span style={{ color: "green", fontWeight: "600" }}>Order Delivered</span>
                                                                        : <span style={{ color: "red", fontWeight: "600" }}>Order Canceled</span>

                                                            }<span style={{ marginLeft: "10px" }} onClick={() => this.viewDetails(obj)} data-toggle="tooltip" data-placement="top" title="" data-original-title="View Details"><BsInfoCircle className="iconI" /></span> </td>
                                                    </tr>


                                                )
                                                : <tr><td colSpan="7"><center>No Trade Found</center></td></tr>
                                        }

                                    </tbody>
                                </table>
                            </form>
                        </div>

                    </div>
                    <div class="row marqueeDiv">
                        <div class="col-md-12 mb-4 mb-md-0">
                            <marquee scrollamount="20" class="marqueeDivMar">
                                {
                                    this.state.marquee.map((obj, i) =>
                                        <>

                                            <div class="customH1 basvamn bgtyd">
                                                {obj.message}
                                            </div>
                                        </>
                                    )
                                }

                            </marquee>
                        </div>
                    </div>


                </div>
                <Modal open={this.state.modalStatus} onClose={this.onCloseModal} center>
                    <div className="customModal">
                        <h5>Order Details</h5>
                        <div className="row" style={{ margin: 0 }}>
                            <table style={{ width: "100%", fontSize: "14px" }} className="table cuf">
                                <tr>
                                    <td style={{ width: "80%" }}>
                                        {this.state.viewDetailsArr.indexTitle}
                                        <span style={{ fontSize: "11px" }}><br /> {this.state.viewDetailsArr.indexQuantity}{this.state.viewDetailsArr.quantityType} X {this.state.viewDetailsArr.liveRate}/10gm</span>
                                    </td>
                                    <td style={{ float: "right" }}> &#8377;{this.state.viewDetailsArr.quantityType == 'KG' ? Math.round(((this.state.viewDetailsArr.liveRate) / 10) * (1000 * this.state.viewDetailsArr.indexQuantity)) : Math.round(((this.state.viewDetailsArr.liveRate) / 10) * this.state.viewDetailsArr.indexQuantity)}
                                    </td>
                                </tr>
                                {
                                    this.state.viewDetailsArr.discount > 0 ?

                                        <tr>
                                            <td>Discount
                                                <span style={{ fontSize: "11px" }}><br />&#8377;{this.state.viewDetailsArr.discount}/10gm</span>
                                            </td>
                                            <td style={{ float: "right" }}> - &#8377;{this.state.viewDetailsArr.quantityType == 'KG' ? Math.round(((this.state.viewDetailsArr.discount) / 10) * (1000 * this.state.viewDetailsArr.indexQuantity)) : Math.round(((this.state.viewDetailsArr.discount) / 10) * this.state.viewDetailsArr.indexQuantity)}</td>
                                        </tr> : null}
                                <tr>
                                    <td>Total - <b>{this.state.viewDetailsArr.indexQuantity}{this.state.viewDetailsArr.quantityType}</b></td>
                                    <td style={{ float: "right" }}><b> &#8377;{this.state.viewDetailsArr.quantityType == 'KG' ? Math.round(((this.state.viewDetailsArr.liveRate - this.state.viewDetailsArr.discount) / 10) * (1000 * this.state.viewDetailsArr.indexQuantity)) : Math.round(((this.state.viewDetailsArr.liveRate - this.state.viewDetailsArr.discount) / 10) * this.state.viewDetailsArr.indexQuantity)}</b></td>
                                </tr>
                                {
                                    this.state.viewDetailsDeliveryArr.length ?
                                        <>
                                            <tr>
                                                <td colSpan="2">Delivery Details</td>
                                            </tr>
                                            {
                                                this.state.viewDetailsDeliveryArr.map((obj, i) =>
                                                    <tr>
                                                        <td>
                                                            <span style={{ fontSize: "11px" }}>{Moment(obj.date).format('f')}</span><br />
                                                            <span style={{ fontSize: "11px" }}>Delivered To : {obj.authorizePerson}</span><br />
                                                            <span>Quantity : <b>{obj.deliveredQuantity}{obj.deliveredQuantityType}</b></span><br />
                                                            <span style={{ fontSize: "11px" }}>Remarks : {obj.remarks}</span>
                                                        </td>
                                                        <td style={{ float: "right" }}>
                                                            &#8377;{obj.paymentReceived}
                                                        </td>
                                                    </tr>

                                                )
                                            }
                                            <tr>
                                                <td>Due - <b>{this.state.viewDetailsArr.remaningQty}{this.state.viewDetailsArr.remaningQtyType}</b></td>
                                                <td style={{ float: "right" }}><b> &#8377;{Math.round(this.state.viewDetailsArr.remaningAmount)}</b></td>
                                            </tr>
                                        </>
                                        :
                                        null
                                }
                            </table>

                        </div>
                    </div>
                </Modal>

            </main>
        )
    }
}
