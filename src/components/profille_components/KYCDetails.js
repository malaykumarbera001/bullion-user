import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { authget, formpost } from '../../utils/service';
import { ToastContainer, toast } from 'react-toastify';
import swal from 'sweetalert';

export default class KYCDetails extends Component {
    constructor(props) {
        super(props)

        this.state = {
            status: false,
            msg: '',
            kyc: [],
            loading: false,
            GSTCertificate: '',
            GSTNumber: '',
            accountHolderName: '',
            accountNumber: '',
            address: '',
            authorizePerson1: '',
            authorizePerson1Photo: '',
            authorizePerson2: '',
            authorizePerson2Photo: '',
            authorizePerson3: '',
            authorizePerson3Photo: '',
            authorizePerson4: '',
            authorizePerson4Photo: '',
            bankName: '',
            branchName: '',
            city: '',
            communicationAddressProof: '',
            companyPAN: '',
            emailID: '',
            ifscCode: '',
            memorandumOfAssociation: '',
            mobileNumber: '',
            name: '',
            natureOfBussiness: '',
            pincode: '',

        }
    }

    componentDidMount() {
        document.title = 'KYC Details | Pawar Bullion'
        window.scrollTo(0, 0)
        this.checkKYC()
        this.getKYC()



    }
    imageCheck = (e) => {
        var formData = new FormData();
        var id = e.target.id
        var file = document.getElementById(id).files[0];
        formData.append("Filedata", file);
        var t = file.type.split('/').pop().toLowerCase();
        if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
            // toast.error('Please select a valid image file');
            toast.dark('Please select a valid image file', {
                position: "bottom-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            document.getElementById(id).value = '';
            return false;
        }
        if (file.size > 1024000) {
            toast.error('Max Upload size is 1MB only');
            document.getElementById(id).value = '';
            return false;
        }
        return true;
    }
    on = e => {
        swal({
            title: "KYC Submitted Sucessfully",
            text: "Please wait for admin approval.",
            icon: "success"
        }).then((value) => {
            this.props.history.push("/")
        });
    }
    checkKYC = () => {
        authget('/user/checkKYC/')
            .then((res) => {
                // console.log(res)
                if (res.data.status) {
                    this.setState({ msg: res.data.msg })
                } else {
                    this.setState({ msg: res.data.msg })

                }
            }).catch((err) => {
                console.log(err)
            })
    }
    getKYC = () => {
        authget('/user/getKYC/')
            .then((res) => {
                console.log(res)
                // console.log(res.data.KYC.name)
                this.setState({
                    kyc: res.data.KYC,
                    status: res.data.status,
                    GSTCertificate: res.data.KYC.GSTCertificate,
                    GSTNumber: res.data.KYC.GSTNumber,
                    accountHolderName: res.data.KYC.accountHolderName,
                    accountNumber: res.data.KYC.accountNumber,
                    address: res.data.KYC.address,
                    authorizePerson1: res.data.KYC.authorizePerson1,
                    authorizePerson1Photo: res.data.KYC.authorizePerson1Photo,
                    authorizePerson2: res.data.KYC.authorizePerson2,
                    authorizePerson2Photo: res.data.KYC.authorizePerson2Photo,
                    authorizePerson3: res.data.KYC.authorizePerson3,
                    authorizePerson3Photo: res.data.KYC.authorizePerson3Photo,
                    authorizePerson4: res.data.KYC.authorizePerson4,
                    authorizePerson4Photo: res.data.KYC.authorizePerson4Photo,
                    bankName: res.data.KYC.bankName,
                    branchName: res.data.KYC.branchName,
                    city: res.data.KYC.city,
                    communicationAddressProof: res.data.KYC.communicationAddressProof,
                    companyPAN: res.data.KYC.companyPAN,
                    emailID: res.data.KYC.emailID,
                    ifscCode: res.data.KYC.ifscCode,
                    memorandumOfAssociation: res.data.KYC.memorandumOfAssociation,
                    mobileNumber: res.data.KYC.mobileNumber,
                    name: res.data.KYC.name,
                    natureOfBussiness: res.data.KYC.natureOfBussiness,
                    pincode: res.data.KYC.pincode,
                    companyName: res.data.KYC.companyName,


                })

            }).catch((err) => {
                console.log(err)
            })
    }
    handleKycSubmit = e => {
        e.preventDefault()
        this.setState({
            loading: true
        })
        var formdata = new FormData(e.target);
        formpost('/user/kycUpload/', formdata)
            .then((res) => {
                this.setState({
                    loading: false
                })
                if (res.data.status) {
                    this.checkKYC()
                    this.getKYC()
                    swal({
                        title: res.data.message,
                        text: "KYC under Review it will take 24 hrs. to update.",
                        icon: "success"
                    }).then((value) => {
                        // this.props.history.push("/")
                    });
                    // toast.dark(res.data.msg, {
                    //     position: "bottom-center",
                    //     autoClose: 5000,
                    //     hideProgressBar: false,
                    //     closeOnClick: true,
                    //     pauseOnHover: true,
                    //     draggable: true,
                    //     progress: undefined,
                    // });
                } else {
                    toast.error(res.data.msg, {
                        position: "bottom-center",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }
                console.log(res)
            }).catch((err) => {
                console.log(err)
            })

    }
    render() {
        return (
            <main id="content" role="main">
                <div class="bg-gray-13 bg-md-transparent">
                    <div class="container">
                        <div class="my-md-3">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><Link to="/">Home</Link></li>
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">KYC Details</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="mb-8 text-center">
                        <h1>KYC Details</h1>
                    </div>
                    {
                        this.state.msg == "KYC Verified" ?
                            <div class="alert alert-success" role="alert">
                                KYC Status : <b>{this.state.msg}</b>
                            </div> : this.state.msg == "KYC Under Review" ?
                                <div class="alert alert-warning" role="alert">
                                    KYC Status : <b>{this.state.msg}</b>
                                </div> :
                                <div class="alert alert-danger" role="alert">
                                    KYC Status : <b>{this.state.msg}</b>
                                </div>
                    }
                    <form onSubmit={this.handleKycSubmit}>

                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="orderid">Name* </label>
                                    <input type="text" required class="form-control" name="name" id="name" defaultValue={this.state.name} placeholder="Enter Your Name" />
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="orderid">Company Name* </label>
                                    <input type="text" required class="form-control" name="companyName" id="companyName" defaultValue={this.state.companyName} placeholder="Enter Your Company Name" />
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="orderid">Phone Number*</label>
                                    <input type="number" required class="form-control" name="mobileNumber" id="mobileNumber" defaultValue={this.state.mobileNumber} placeholder="Enter Your Phone Number" />
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="orderid">Email ID*</label>
                                    <input type="email" required class="form-control" name="emailID" id="emailID" defaultValue={this.state.emailID} placeholder="Enter Your Email ID" />
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="orderid">Address*</label>
                                    <input type="text" required class="form-control" name="address" id="address" defaultValue={this.state.address} placeholder="Enter Your Address" />
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="orderid">City*</label>
                                    <input type="text" required class="form-control" name="city" id="city" defaultValue={this.state.city} placeholder="Enter Your City" />
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="orderid">Pincode*</label>
                                    <input type="text" required class="form-control" name="pincode" id="pincode" defaultValue={this.state.pincode} placeholder="Enter Your Pincode" />
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="orderid">GST Number*</label>
                                    <input type="text" required class="form-control" name="GSTNumber" id="GSTNumber" defaultValue={this.state.GSTNumber} placeholder="Enter Your GST Number" />
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="orderid">Nature of Bussiness*</label>
                                    <input type="text" required class="form-control" name="natureOfBussiness" id="natureOfBussiness" defaultValue={this.state.natureOfBussiness} placeholder="Enter Nature of Bussiness" />
                                </div>
                            </div>
                            {/* <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="orderid">Account Number*</label>
                                    <input type="text" required class="form-control" name="accountNumber" id="accountNumber" defaultValue={this.state.accountNumber} placeholder="Enter Your Account Number" />
                                </div>
                            </div> */}
                            {/* <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="orderid">Bank Name*</label>
                                    <input type="text" required class="form-control" name="bankName" id="bankName" defaultValue={this.state.bankName} placeholder="Enter Your Bank Name" />
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="orderid">IFSC Code*</label>
                                    <input type="text" required class="form-control" name="ifscCode" id="ifscCode" defaultValue={this.state.ifscCode} placeholder="EnterYour Bank IFSC Code" />
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="orderid">Account Holder Name*</label>
                                    <input type="text" required class="form-control" name="accountHolderName" defaultValue={this.state.accountHolderName} id="accountHolderName" placeholder="Enter Account Holder Name" />
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="orderid">Branch Name*</label>
                                    <input type="text" required class="form-control" name="branchName" defaultValue={this.state.branchName} id="branchName" placeholder="Enter Bank Branch Name" />
                                </div>
                            </div>
                          */}
                            <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="orderid">Authorize Person 1*</label>
                                    <input type="text" required class="form-control" name="authorizePerson1" defaultValue={this.state.authorizePerson1} id="authorizePerson1" placeholder="Enter Authorize Person 1 Name" />
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="orderid">Authorize Person 2</label>
                                    <input type="text" class="form-control" name="authorizePerson2" defaultValue={this.state.authorizePerson2} id="authorizePerson2" placeholder="Enter Authorize Person 2 Name" />
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="orderid">Authorize Person 3</label>
                                    <input type="text" class="form-control" name="authorizePerson3" defaultValue={this.state.authorizePerson3} id="authorizePerson3" placeholder="Enter Authorize Person 3 Name" />
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="orderid">Authorize Person 4</label>
                                    <input type="text" class="form-control" name="authorizePerson4" defaultValue={this.state.authorizePerson4} id="authorizePerson4" placeholder="Enter Authorize Person 4 Name" />
                                </div>
                            </div>
                            {
                                this.state.status == true ?
                                    <div class="col-md-6 mb-3">
                                        <div class="js-form-message form-group">
                                            <label class="form-label">Company PAN</label>
                                            <input type="file" class="form-control" name="companyPAN" id="companyPAN" onChange={this.imageCheck} />
                                        </div>
                                    </div>
                                    : <div class="col-md-6 mb-3">
                                        <div class="js-form-message form-group">
                                            <label class="form-label">Company PAN*</label>
                                            <input type="file" required class="form-control" name="companyPAN" id="companyPAN" onChange={this.imageCheck} />
                                        </div>
                                    </div>
                            }
                            {
                                this.state.status == true ?
                                    <div class="col-md-6 mb-3">
                                        <div class="js-form-message form-group">
                                            <label class="form-label">GST Certificate</label>
                                            <input type="file" class="form-control" name="GSTCertificate" id="GSTCertificate" onChange={this.imageCheck} />
                                        </div>
                                    </div>
                                    :
                                    <div class="col-md-6 mb-3">
                                        <div class="js-form-message form-group">
                                            <label class="form-label">GST Certificate*</label>
                                            <input type="file" required class="form-control" name="GSTCertificate" id="GSTCertificate" onChange={this.imageCheck} />
                                        </div>
                                    </div>
                            }

                            {
                                this.state.status == true ?
                                    <div class="col-md-6 mb-3">
                                        <div class="js-form-message form-group">
                                            <label class="form-label">Authorize Person 1 Photo</label>
                                            <input type="file" class="form-control" name="authorizePerson1Photo" id="authorizePerson1Photo" onChange={this.imageCheck} />
                                        </div>
                                    </div>
                                    :
                                    <div class="col-md-6 mb-3">
                                        <div class="js-form-message form-group">
                                            <label class="form-label">Authorize Person 1 Photo*</label>
                                            <input type="file" required class="form-control" name="authorizePerson1Photo" id="authorizePerson1Photo" onChange={this.imageCheck} />
                                        </div>
                                    </div>
                            }
                            <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label">Authorize Person 2 Photo</label>
                                    <input type="file" class="form-control" name="authorizePerson2Photo" id="authorizePerson2Photo" onChange={this.imageCheck} />
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label">Authorize Person 3 Photo</label>
                                    <input type="file" class="form-control" name="authorizePerson3Photo" id="authorizePerson3Photo" onChange={this.imageCheck} />
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="js-form-message form-group">
                                    <label class="form-label">Authorize Person 4 Photo</label>
                                    <input type="file" class="form-control" name="authorizePerson4Photo" id="authorizePerson4Photo" onChange={this.imageCheck} />
                                </div>
                            </div>
                            {/* {
                                String(this.state.communicationAddressProof).search('/uploads/') > 0 ?
                                    <div class="col-md-4 mb-3">
                                        <label class="form-label">Communication Address Proof*</label>
                                        <img src={this.state.communicationAddressProof} style={{ width: "100%" }} />
                                    </div>
                                    :
                                    <div class="col-md-6 mb-3">
                                        <div class="js-form-message form-group">
                                            <label class="form-label">Communication Address Proof*</label>

                                            <input type="file" required class="form-control" name="communicationAddressProof" id="communicationAddressProof" />
                                        </div>
                                    </div>
                            } */}
                        </div>
                        <div className="row">
                            {
                                String(this.state.companyPAN).search('/uploads/') > 0 ?

                                    <div class="col-md-3 mb-3">
                                        <div class="js-form-message form-group">
                                            <label class="form-label">Company PAN*</label>
                                            <img src={this.state.companyPAN} style={{ width: "100%" }} />
                                        </div>
                                    </div>
                                    :
                                    null
                            }
                            {/* {
                                String(this.state.memorandumOfAssociation).search('/uploads/') > 0 ?
                                    <div class="col-md-4 mb-3">
                                        <div class="js-form-message form-group">
                                            <label class="form-label">Memorandum Of Association*</label>
                                            <img src={this.state.memorandumOfAssociation} style={{ width: "100%" }} />
                                        </div>
                                    </div>
                                    :
                                    <div class="col-md-6 mb-3">
                                        <div class="js-form-message form-group">
                                            <label class="form-label">Memorandum Of Association*</label>
                                            <input type="file" required class="form-control" name="memorandumOfAssociation" id="memorandumOfAssociation" />
                                        </div>
                                    </div>
                            } */}
                            {
                                String(this.state.GSTCertificate).search('/uploads/') > 0 ?
                                    <div class="col-md-3 mb-3">
                                        <div class="js-form-message form-group">
                                            <label class="form-label">GST Certificate*</label>
                                            <img src={this.state.GSTCertificate} style={{ width: "100%" }} />

                                        </div>
                                    </div>
                                    :
                                    null
                            }
                            {
                                this.state.status == true ?
                                    String(this.state.authorizePerson1Photo).search('/uploads/') > 0 ?

                                        <div class="col-md-3 mb-3">
                                            <div class="js-form-message form-group">
                                                <label class="form-label">Authorize Person 1 Photo*</label>
                                                <img src={this.state.authorizePerson1Photo} style={{ width: "100%" }} />
                                            </div>
                                        </div>
                                        :
                                        null
                                    : null

                            }
                            {
                                this.state.status == true ?

                                    String(this.state.authorizePerson2Photo).search('/uploads/') > 0 ?

                                        <div class="col-md-3 mb-3">
                                            <div class="js-form-message form-group">
                                                <label class="form-label">Authorize Person 2 Photo</label>
                                                <img src={this.state.authorizePerson2Photo} style={{ width: "100%" }} />
                                            </div>
                                        </div>
                                        :
                                        null
                                    : null
                            }
                            {
                                this.state.status == true ?

                                    String(this.state.authorizePerson3Photo).search('/uploads/') > 0 ?


                                        <div class="col-md-3 mb-3">
                                            <div class="js-form-message form-group">
                                                <label class="form-label">Authorize Person 3 Photo</label>
                                                <img src={this.state.authorizePerson3Photo} style={{ width: "100%" }} />
                                            </div>
                                        </div>
                                        :
                                        null
                                    :
                                    null

                            }

                            {
                                this.state.status == true ?

                                    String(this.state.authorizePerson4Photo).search('/uploads/') > 0 ?

                                        <div class="col-md-3 mb-3">
                                            <div class="js-form-message form-group">
                                                <label class="form-label">Authorize Person 4 Photo</label>
                                                <img src={this.state.authorizePerson4Photo} style={{ width: "100%" }} /> </div>
                                        </div>
                                        :
                                        null
                                    :
                                    null
                            }
                        </div>
                        {
                            this.state.status == true ?


                                <div class=" col mb-1  mb-12">
                                    {/* <center><button type="submit" class="btn btn-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-4 px-lg-5 w-100 w-md-auto" disabled={this.state.loading}>{this.state.loading ? <>Loading...</> : <>Update KYC</>}</button></center> */}
                                </div>
                                :
                                <div class="col mb-1  mb-12">
                                    <center><button type="submit" class="btn btn-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-4 px-lg-5 w-100 w-md-auto" disabled={this.state.loading}>{this.state.loading ? <>Loading...</> : <>Submit KYC</>}</button></center>
                                </div>
                        }

                    </form>


                </div>
                <br /><br />
                <ToastContainer />
            </main>
        )
    }
}
