import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Avatar from 'react-avatar';
import { checkAuth } from '../../utils/auth';
import {connect} from 'react-redux';
import {Show_name} from '../auth_components/action';
import { authget } from '../../utils/service';
class MyAccount extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             status:'',
             msg:''
        }
    }
    
    componentDidMount(){
        window.title='My Account | Pawar Bullion'
        window.scrollTo(0,0)
        authget('/user/checkKYC/')
        .then((res) => {
            console.log(res)
            if(res.data.status){
                this.setState({status:true,msg:res.data.msg})
            }else{
                this.setState({status:false,msg:res.data.msg})

            }
        }).catch((err) => {
            console.log(err)
        })

    }
    handleLogout = ()=>{
        checkAuth.signout();
        this.props.Profile_action();
        localStorage.removeItem('pawarBullionUserToken');
        localStorage.removeItem('pawarBullionRefreshToken');
        localStorage.removeItem('pawarBullionPhone');
        localStorage.removeItem('pawarBullionEmail');
        localStorage.removeItem('pawarBullionName');
        localStorage.removeItem('pawarBullionId');
        this.props.history.push('/login')

    }
    render() {
        return (
            <main id="content" role="main">
            <div class="bg-gray-13 bg-md-transparent">
                <div class="container">
                    <div class="my-md-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><Link to="/">Home</Link></li>
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">My Account</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="mb-8 text-center">
                <Avatar
                                                    name={String(localStorage.getItem('pawarBullionName'))}
                                                    size="150"
                                                    round="100px"
                                                /><br/><br/>

                    <h2>{String(localStorage.getItem('pawarBullionName'))}</h2>
                    <h4>{String(localStorage.getItem('pawarBullionEmail'))}</h4>
                    <h4>+91 {String(localStorage.getItem('pawarBullionPhone'))}</h4>
                    <br/>
                    <div>
                        <h3>KYC Status : {this.state.msg}</h3>
                    </div>
                    <br/>
                    <button class="btn btn-secondary" style={{width:"150px",cursor:"pointer"}} onClick={this.handleLogout}>Logout</button>
                </div>
                
            </div>
            </main>
        )
    }
}

const mapStateToProps=(state)=>{
	// console.log(state.profile);
	

	return{
	  profile_details: state.profile,
	}
  }

  const mapDispatchToProps=(dispatch)=>{
    return{
        Profile_action:()=>{ dispatch(Show_name())},
    
    }
}

  
export default connect(mapStateToProps,mapDispatchToProps) (MyAccount);


