import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import queryString from 'query-string';
import { authpost, get } from '../../utils/service';
import firebase from '../../utils/firebase'
import { ToastContainer, toast } from 'react-toastify';

var urlCrypt = require('url-crypt')('~{ry*I)==yU/]9<7DPk!Hj"R#:-/Z7(hTBnlRS=4CXF');

export default class ModifyPendingOrder extends Component {
    constructor(props) {
        super(props)
        let url = this.props.location.search;
        let params = queryString.parse(url);
        this.state = {
            trade: [],
            id: urlCrypt.decryptObj(params.NgTh),
            addAskAmount: 0,
            addBidAmount: 0,
            goldMCX: [],
            goldMCXBidColor: 'eq',
            goldMCXAskColor: 'eq',
            targetPrice: 0,
            quantity: [],
            selectquantity: 0,
            marquee: []


        }
    }

    componentDidMount() {
        document.title = 'Modify Pending Order| Pawar Bullion'
        window.scrollTo(0, 0)
        this.getTrade()
        this.getLiveRate()
        this.getMarquee()


    }
    getMarquee = () => {
        get('/user/notification/')
            .then((res) => {
                console.log(res)
                this.setState({
                    marquee: res.data.filter(word => word.page == 'Pending Order')
                })
            }).catch((err) => {
                console.log(err)
            })
    }
    handleClick = (type) => {
        this.setState({
            type: type
        })
    }
    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }
    getLiveRate = () => {

        var db = firebase.firestore();



        db.collection("LiveRateNewAPI").doc("DGxd1BbSKTzGML1cvOB3")
            .onSnapshot((doc) => {
                var today = new Date();
                var time = today.getSeconds() + ":" + today.getMilliseconds();

                console.log("Current data at : " + time, doc.data());
                var todos = doc.data()
                // console.log(todos)
                // this.setState({
                //     goldMCXBidColor: 'eq',
                //     silverMCXBidColor: 'eq',
                //     goldColor: 'eq',
                //     silverColor: 'eq',
                //     INRColor: 'eq',
                //     goldMCXAskColor: 'eq',
                //     silverMCXAskColor: 'eq',
                // })
                var goldMCXAskOld = this.state.goldMCX['sell']
                if (goldMCXAskOld > todos['rows']['GOLD']['sell']) {
                    this.setState({
                        goldMCXAskColor: 'down'
                    })
                } else if (goldMCXAskOld < todos['rows']['GOLD']['sell']) {
                    this.setState({
                        goldMCXAskColor: 'up'
                    })
                } else {
                    this.setState({
                        goldMCXAskColor: 'eq'
                    })
                }
                var goldMCXBidOld = this.state.goldMCX['buy']
                if (goldMCXBidOld > todos['rows']['GOLD']['buy']) {
                    this.setState({
                        goldMCXBidColor: 'down'
                    })
                } else if (goldMCXBidOld < todos['rows']['GOLD']['buy']) {
                    this.setState({
                        goldMCXBidColor: 'up'
                    })
                } else {
                    this.setState({
                        goldMCXBidColor: 'eq'
                    })
                }

                try {
                    this.setState({
                        goldMCX: todos['rows']['GOLD'],

                    })
                }
                catch (err) {
                    console.log(err.message)
                }

            });

    }
    getTrade = () => {
        authpost('/order/futureOrder/' + this.state.id)
            .then((res) => {
                console.log(res)
                this.setState({
                    trade: res.data,
                    targetPrice: res.data.rate,
                    selectquantity: res.data.indexQuantityID,

                    quantity: res.data.otherQuantity,
                    addAskAmount: res.data.addAskAmount,
                    addBidAmount: res.data.addBidAmount,
                })
            }).catch((err) => {
                console.log(err)
            })
    }
    handleModify = (e) => {
        const data = {
            'id': this.state.id,
            'indexID': this.state.trade.indexID,
            'indexQuantityID': this.state.selectquantity,
            'rate': this.state.targetPrice
        }
        authpost('/order/updateFutureOrder/', data)
            .then((res) => {
                console.log(res)
                if (res.data.status) {
                    toast.dark(res.data.msg, {
                        position: "bottom-center",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    setTimeout(() => { this.props.history.push('/pendingOrder') }, 2000);
                } else {
                    toast.error(res.data.msg, {
                        position: "bottom-center",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }
            }).catch((err) => {
                console.log(err)
            })
    }
    render() {
        return (
            <div>
                <div class="bg-gray-13 bg-md-transparent">
                    <div class="container">
                        <div class="my-md-3">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><Link to="/">Home</Link></li>
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><Link to="/pendingOrder">Pending Order</Link></li>
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Modify</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="mb-12 text-center" >
                    <h1>Modify Order</h1>
                    <div class="align-middle"><div class="customH5 font-weight-bold">{this.state.trade.indexTitle}</div></div>

                    <div class="cutomRow row " >

                        <div class="customWidth" style={{ padding: "0 10px 0 0" }}>
                            <div class="card shadow text-center customeDiv">
                                <div class="text mt-2 ">
                                    <h4 class="mb-0 Rates  customRow"><span className={this.state.goldMCXBidColor}>{parseInt(this.state.goldMCX['buy']) + parseInt(this.state.addBidAmount)}</span></h4>
                                    <label class="text-truncate font-weight-bold" style={{ margin: 0 }}>Buy</label>
                                </div>
                            </div>
                        </div>
                        <div class="customWidth" style={{ padding: "0 10px 0 0" }}>
                            <div class="card shadow text-center customeDiv">
                                <div class="text mt-2 ">
                                    <h4 class="mb-0 Rates  customRow"><span className={this.state.goldMCXAskColor}>{parseInt(this.state.goldMCX['sell']) + parseInt(this.state.addAskAmount)}</span></h4>
                                    <label class="text-truncate font-weight-bold" style={{ margin: 0 }}>Sell</label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <center style={{ width: "100%" }}>
                            <table>
                                <tr>
                                    <td>
                                        <h5 class="fs-4 text text-truncate font-weight-bold">Quantity : </h5>
                                    </td>
                                    <td>

                                        <select class="form-control" name="selectquantity" style={{ width: "150px", marginLeft: "10px" }} onChange={this.handleChange}>
                                            {
                                                this.state.quantity.map((obj, i) =>
                                                    obj.selected ?
                                                        <option value={obj.id} selected>{obj.number}{obj.numberType}</option>
                                                        :
                                                        <option value={obj.id}>{obj.number}{obj.numberType}</option>

                                                )
                                            }

                                        </select>


                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h5 class="fs-4 text text-truncate font-weight-bold">Price : </h5>

                                    </td>
                                    <td>
                                        <input name="targetPrice" onChange={this.handleChange} type="number" class="form-control" placeholder="Enter Price" value={this.state.targetPrice} style={{ width: "150px", marginLeft: "10px" }} />
                                    </td>
                                </tr>

                                <tr >
                                    <td colSpan="2" align="center"><br /><button class="btn btn-primary" style={{ cursor: "pointer" }} onClick={this.handleModify}>Modify Order</button></td>
                                </tr>
                            </table>

                        </center>
                    </div>
                </div>

                <div class="container" style={{ marginBottom: "5vw" }}>
                    <hr />
                    <div class="row">
                        <div class="col-md-12 mb-4 mb-md-0">
                            <marquee scrollamount="20" class="marqueeDivMar">
                                {
                                    this.state.marquee.map((obj, i) =>
                                        <>

                                            <div class="customH1 basvamn bgtyd">
                                                {obj.message}
                                            </div>
                                        </>
                                    )
                                }

                            </marquee>
                        </div>
                    </div>
                </div>
                <ToastContainer />

            </div>
        )
    }
}
