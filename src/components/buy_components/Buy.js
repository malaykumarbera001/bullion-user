import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import queryString from 'query-string';
import firebase from '../../utils/firebase'
import { authget, authpost, get } from '../../utils/service';
import { ToastContainer, toast } from 'react-toastify';
import swal from 'sweetalert';
var urlCrypt = require('url-crypt')('~{ry*I)==yU/]9<7DPk!Hj"R#:-/Z7(hTBnlRS=4CXF');
export default class Buy extends Component {
    constructor(props) {
        super(props)
        let url = this.props.location.search;
        let params = queryString.parse(url);
        console.log(params)
        this.state = {
            type: 'Market',
            addAskAmount: urlCrypt.decryptObj(params.NhTF),
            addBidAmount: urlCrypt.decryptObj(params.Htdf),
            id: urlCrypt.decryptObj(params.GfT),
            title: urlCrypt.decryptObj(params.NmlJhTF),
            goldMCX: [],
            goldMCXBidColor: 'eq',
            goldMCXAskColor: 'eq',
            viewIndexQuantity: [],
            loading: false,
            marquee: [],
            targetPrice: 0,
            loading: false,
            selectquantity: 0



        }
    }

    componentDidMount() {
        document.title = 'Buy Gold | Pawar Bullion'
        window.scrollTo(0, 0)
        this.getLiveRate()
        this.getQuantity(this.state.id)
        this.getMarquee()
    }
    getMarquee = () => {
        get('/user/notification/')
            .then((res) => {
                console.log(res)
                this.setState({
                    marquee: res.data.filter(word => word.page == 'Buy')
                })
            }).catch((err) => {
                console.log(err)
            })
    }
    handleClick = (type) => {
        this.setState({
            type: type
        })


    }
    getLiveRate = () => {

        var db = firebase.firestore();



        db.collection("LiveRateNewAPI").doc("DGxd1BbSKTzGML1cvOB3")
            .onSnapshot((doc) => {
                var today = new Date();
                var time = today.getSeconds() + ":" + today.getMilliseconds();

                console.log("Current data at : " + time, doc.data());
                var todos = doc.data()

                // this.setState({
                //     goldMCXBidColor: 'eq',
                //     silverMCXBidColor: 'eq',
                //     goldColor: 'eq',
                //     silverColor: 'eq',
                //     INRColor: 'eq',
                //     goldMCXAskColor: 'eq',
                //     silverMCXAskColor: 'eq',
                // })
                var goldMCXAskOld = this.state.goldMCX['sell']
                if (goldMCXAskOld > todos['rows']['GOLD']['sell']) {
                    this.setState({
                        goldMCXAskColor: 'down'
                    })
                } else if (goldMCXAskOld < todos['rows']['GOLD']['sell']) {
                    this.setState({
                        goldMCXAskColor: 'up'
                    })
                } else {
                    this.setState({
                        goldMCXAskColor: 'eq'
                    })
                }
                var goldMCXBidOld = this.state.goldMCX['buy']
                if (goldMCXBidOld > todos['rows']['GOLD']['buy']) {
                    this.setState({
                        goldMCXBidColor: 'down'
                    })
                } else if (goldMCXBidOld < todos['rows']['GOLD']['buy']) {
                    this.setState({
                        goldMCXBidColor: 'up'
                    })
                } else {
                    this.setState({
                        goldMCXBidColor: 'eq'
                    })
                }

                try {
                    this.setState({
                        goldMCX: todos['rows']['GOLD'],

                    })
                }
                catch (err) {
                    console.log(err.message)
                }

            });

    }
    getQuantity = (id) => {
        get('/user/quantity/' + id)
            .then((res) => {
                console.log(res)
                this.setState({
                    viewIndexQuantity: res.data
                })
            }).catch((err) => {
                console.log(err)
            })
    }
    handleOrder = () => {

        if (this.state.selectquantity == 0) {
            toast.error('Please select Quantity', {
                position: "bottom-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            this.setState({
                loading: false
            })
        } else {
            if (this.state.type == 'Pending') {
                if (this.state.targetPrice == 0) {
                    toast.error('Please enter target price', {
                        position: "bottom-center",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    this.setState({
                        loading: false
                    })
                } else {
                    this.setState({
                        loading: true
                    })
                    const data = {
                        'indexID': this.state.id,
                        'indexQuantityID': this.state.selectquantity,
                        'rate': this.state.targetPrice
                    }
                    authpost('/order/createFutureOrder/', data)
                        .then((res) => {
                            if (res.data.status) {
                                this.setState({
                                    loading: false
                                })
                                // toast.dark(res.data.msg, {
                                //     position: "bottom-center",
                                //     autoClose: 5000,
                                //     hideProgressBar: false,
                                //     closeOnClick: true,
                                //     pauseOnHover: true,
                                //     draggable: true,
                                //     progress: undefined,
                                // });
                                // setTimeout(this.getAlert('Pending Order Sucessfully'), 2000);
                                swal('Pending Order Placed Successfully', "Any Query please contact to 74398 84721.", "success").then((value) => {
                                    this.props.history.push("/pendingOrder")
                                });

                            } else {
                                this.setState({
                                    loading: false
                                })
                                swal(res.data.msg, "Any Quary please contact to 74398 84721.", "warning");

                                // swal({
                                //     title: res.data.msg,
                                //     icon: "success",
                                //   })
                                // toast.error(res.data.msg, {
                                //     position: "bottom-center",
                                //     autoClose: 5000,
                                //     hideProgressBar: false,
                                //     closeOnClick: true,
                                //     pauseOnHover: true,
                                //     draggable: true,
                                //     progress: undefined,
                                // });
                            }
                        }).catch((err) => {
                            console.log(err)
                        })
                }
            } else {
                const data = {
                    'indexID': this.state.id,
                    'indexQuantityID': this.state.selectquantity,
                    // 'rate':this.state.targetPrice
                }
                authpost('/order/createOrder/', data)
                    .then((res) => {
                        if (res.data.status) {
                            this.setState({
                                loading: false
                            })
                            // toast.dark(res.data.msg, {
                            //     position: "bottom-center",
                            //     autoClose: 5000,
                            //     hideProgressBar: false,
                            //     closeOnClick: true,
                            //     pauseOnHover: true,
                            //     draggable: true,
                            //     progress: undefined,
                            // });
                            swal('Order Confirmed', "Any Query please contact to 74398 84721.", "success").then((value) => {
                                this.props.history.push("/trade")
                            });

                            // setTimeout(this.getAlert('Order Confirmed'), 2000);
                        } else {
                            this.setState({
                                loading: false
                            })
                            // swal({
                            //     title: res.data.msg,
                            //     icon: "success",
                            //   })
                            swal(res.data.msg, "Any Quary please contact to 74398 84721.", "warning");
                            // toast.error(res.data.msg, {
                            //     position: "bottom-center",
                            //     autoClose: 5000,
                            //     hideProgressBar: false,
                            //     closeOnClick: true,
                            //     pauseOnHover: true,
                            //     draggable: true,
                            //     progress: undefined,
                            // });
                        }
                    }).catch((err) => {
                        console.log(err)
                    })
            }
        }
    }
    getAlert = (data) => {
        swal({
            title: data,
            icon: "success",
        })
    }
    checkAuth = () => {
        this.setState({
            loading: true
        })
        authpost('/auth/authCheck/')
            .then((res) => {
                if (res.status != 200) {
                    this.setState({
                        loading: false
                    })
                    toast.dark('Please login yourself..', {
                        position: "bottom-center",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                } else {
                    authget('/user/checkKYC/')
                        .then((res) => {
                            console.log(res)
                            if (res.data.status) {
                                this.handleOrder()
                            } else {
                                swal(res.data.msg, "Any Quary please contact to 74398 84721.", "warning");

                                // toast.dark(res.data.msg, {
                                //     position: "bottom-center",
                                //     autoClose: 5000,
                                //     hideProgressBar: false,
                                //     closeOnClick: true,
                                //     pauseOnHover: true,
                                //     draggable: true,
                                //     progress: undefined,
                                // });
                                this.setState({
                                    loading: false
                                })
                            }
                        }).catch((err) => {
                            console.log(err)
                        })
                }
            }).catch((err) => {
                console.log(err)
            })

    }
    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }
    render() {
        return (
            <div>
                <div class="bg-gray-13 bg-md-transparent">
                    <div class="container">
                        <div class="my-md-3">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><Link to="/">Home</Link></li>
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Buy Gold</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="mb-12 text-center" >
                    <h1>Buy Gold</h1>
                    <div class="align-middle"><div class="customH5 font-weight-bold">{this.state.title}</div></div>
                    <div class="cutomRow" style={{ paddingBottom: 0 }}>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            {
                                this.state.type == 'Market' ?
                                    <>
                                        <button type="button" class="btn btn-primary" onClick={() => this.handleClick('Market')}>Market</button>

                                        <button type="button" class="btn btn-secondary" onClick={() => this.handleClick('Pending')}>Pending</button>

                                    </>
                                    : <>
                                        <button type="button" class="btn btn-secondary" onClick={() => this.handleClick('Market')}>Market</button>

                                        <button type="button" class="btn btn-primary" onClick={() => this.handleClick('Pending')}>Pending</button>

                                    </>
                            }
                        </div>
                    </div>
                    <div class="cutomRow row " >

                        <div class="customWidth" style={{ padding: "0 10px 0 0" }}>
                            <div class="card shadow text-center customeDiv">
                                <div class="text mt-2 ">
                                    <h4 class="mb-0 Rates  customRow"><span className={this.state.goldMCXBidColor}>{parseInt(this.state.goldMCX['buy']) + parseInt(this.state.addBidAmount)}</span></h4>
                                    <label class="text-truncate font-weight-bold" style={{ margin: 0 }}>Buy</label>
                                </div>
                            </div>
                        </div>
                        <div class="customWidth" style={{ padding: "0 10px 0 0" }}>
                            <div class="card shadow text-center customeDiv">
                                <div class="text mt-2 ">
                                    <h4 class="mb-0 Rates  customRow"><span className={this.state.goldMCXAskColor}>{parseInt(this.state.goldMCX['sell']) + parseInt(this.state.addAskAmount)}</span></h4>
                                    <label class="text-truncate font-weight-bold" style={{ margin: 0 }}>Sell</label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <center style={{ width: "100%" }}>
                            <table>
                                <tr>
                                    <td>
                                        <h5 class="fs-4 text text-truncate font-weight-bold">Quantity : </h5>
                                    </td>
                                    <td>
                                        <select class="form-control" name="selectquantity" onChange={this.handleChange} style={{ width: "150px", marginLeft: "10px" }}>
                                            <option disabled selected>Select Weight</option>
                                            {
                                                this.state.viewIndexQuantity.map((obj, i) =>
                                                    <option value={obj.id}>{obj.number}{obj.numberType}</option>



                                                )
                                            }


                                        </select>


                                    </td>
                                </tr>
                                {
                                    this.state.type == 'Pending' ?
                                        <tr>
                                            <td>
                                                <h5 class="fs-4 text text-truncate font-weight-bold">Price : </h5>

                                            </td>
                                            <td>
                                                <input type="number" name="targetPrice" onChange={this.handleChange} class="form-control" placeholder="Enter Price" style={{ width: "150px", marginLeft: "10px" }} />
                                            </td>
                                        </tr>
                                        : null
                                }
                                <tr >
                                    <td colSpan="2" align="center"><br /><button class="btn btn-primary" style={{ width: "100px", cursor: "pointer" }} onClick={this.checkAuth} disabled={this.state.loading}>{this.state.loading == false ? <>Buy</> : <>Loading...</>}</button></td>
                                </tr>
                            </table>

                        </center>
                    </div>
                </div>

                <div class="container" style={{ marginBottom: "5vw" }}>
                    <hr />
                    <div class="row">
                        <div class="col-md-12 mb-4 mb-md-0">
                            <marquee scrollamount="20" class="marqueeDivMar">
                                {
                                    this.state.marquee.map((obj, i) =>
                                        <>

                                            <div class="customH1 basvamn bgtyd">
                                                {obj.message}
                                            </div>
                                        </>
                                    )
                                }

                            </marquee>
                        </div>
                    </div>
                </div>
                <ToastContainer />
            </div>
        )
    }
}
