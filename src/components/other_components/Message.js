import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { get } from '../../utils/service'
import Moment from 'moment-js';
export default class Message extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             message:[]
        }
    }
    
    componentDidMount() {
        document.title = 'Message | Pawar Bullion'
        window.scrollTo(0,0)
        this.getMessage()
    }
    getMessage=()=>{
        get('/user/message/')
        .then((res) => {
            console.log(res)
            this.setState({
                message: res.data.filter(word => word.page =="Message")
            })
        }).catch((err) => {
            console.log(err)
        })
    }
    render() {
        return (
            <main id="content" role="main">
                <div class="bg-gray-13 bg-md-transparent">
                    <div class="container">
                        <div class="my-md-3">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><Link to="/">Home</Link></li>
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Message</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="mb-8 text-center">
                        <h1>Message</h1>
                    </div>
                    <div class="mb-12">
                       
                            {
                                this.state.message.map((obj,i)=>
                                <div class="row meaasgeDiv">
                                    {
                                        String(obj.image).search('/uploads/')>0?
                                        <>
                           <div class="col-md-4 mb-md-0 imageDiv" style={{padding:0}}>
                                    <img class="messageImage" src={obj.image} name={obj.title} alt={obj.title} />
                                </div>
                                <div class="col-md-8 mb-8 mb-md-0  ml-md-auto  mr-md-auto">
                                    <div class="card mb-3 border-0 rounded-0" style={{ padding: "10px 0" }}>
                                        <div><span class="text font-weight-bold font-size-20">{obj.title}</span><span style={{ float: "right" }}>{Moment(obj.date).format('f')}</span></div>
                                        <div class="bankDiv" dangerouslySetInnerHTML={{ __html: obj.message }}>
                                          
                                        </div>
                                    </div>
                                </div>
                                        </>:
                                        <>
                                         <div class="col-md-12 mb-12 mb-md-0  ml-md-auto  mr-md-auto">
                                    <div class="card mb-3 border-0 rounded-0" style={{ padding: "10px 0" }}>
                                        <div><span class="text font-weight-bold font-size-20">{obj.title}</span><span style={{ float: "right" }}>{Moment(obj.date).format('f')}</span></div>
                                        <div class="bankDiv" dangerouslySetInnerHTML={{ __html: obj.message }}>
                                           
                                
                                        </div>
                                    </div>
                                </div>
                                        </>
                                    }
     
    
    
                            </div>
                                )
                            }
                        
                    </div>

                </div>
            </main>

        )
    }
}
