import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class Policy extends Component {
    componentDidMount(){
        document.title = 'Our Policy | Pawar Bullion'
        window.scrollTo(0,0)

     }
    render() {
        return (
            <main id="content" role="main">
            <div class="bg-gray-13 bg-md-transparent">
                <div class="container">
                    <div class="my-md-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><Link to="/">Home</Link></li>
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Our Policy</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="mb-8 text-center">
                    <h1>Our Policy</h1>
                </div>
                <div class="mb-10">
    <h3 class="mb-6 pb-2 font-size-25">Intellectual Propertly</h3>
    <ol>
        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis diam erat. Duis velit lectus, posuere a blandit sit amet, tempor at lorem. Donec ultricies, lorem sed ultrices interdum.</li>
        <li>Leo metus luctus sem, vel vulputate diam ipsum sed lorem. Donec tempor arcu nisl, et molestie massa scelerisque ut. Nunc at rutrum leo. Mauris metus mauris, tristique quis sapien eu, rutrum vulputate enim.</li>
        <li>Mauris tempus erat laoreet turpis lobortis, eu tincidunt erat fermentum.</li>
        <li>Aliquam non tincidunt urna. Integer tincidunt nec nisl vitae ullamcorper. Proin sed ultrices erat. Praesent varius ultrices massa at faucibus.</li>
        <li>Aenean dignissim, orci sed faucibus pharetra, dui mi dignissim tortor, sit amet condimentum mi ligula sit amet augue.</li>
        <li>Pellentesque vitae eros eget enim mollis placerat.</li>
    </ol>
</div>
<div class="mb-10">
                    <h3 class="mb-6 pb-2 font-size-25">Termination</h3>
                    <ol>
                        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis diam erat. Duis velit lectus, posuere a blandit sit amet, tempor at lorem. Donec ultricies, lorem sed ultrices interdum.</li>
                        <li>Leo metus luctus sem, vel vulputate diam ipsum sed lorem. Donec tempor arcu nisl, et molestie massa scelerisque ut. Nunc at rutrum leo. Mauris metus mauris, tristique quis sapien eu, rutrum vulputate enim.</li>
                        <li>Mauris tempus erat laoreet turpis lobortis, eu tincidunt erat fermentum.</li>
                        <li>Aliquam non tincidunt urna. Integer tincidunt nec nisl vitae ullamcorper. Proin sed ultrices erat. Praesent varius ultrices massa at faucibus.</li>
                        <li>Aenean dignissim, orci sed faucibus pharetra, dui mi dignissim tortor, sit amet condimentum mi ligula sit amet augue.</li>
                        <li>Pellentesque vitae eros eget enim mollis placerat.</li>
                    </ol>
                </div>
                <div class="mb-10">
                    <h3 class="mb-6 pb-2 font-size-25">Changes To This Agreement</h3>
                    <p class="text-gray">We reserve the right, at our sole discretion, to modify or replace these Terms and Conditions by posting the updated terms on the Site. Your continued use of the Site after any such changes constitutes your acceptance of the new Terms and Conditions.</p>
                </div>
            </div>
                
            </main>
        )
    }
}
