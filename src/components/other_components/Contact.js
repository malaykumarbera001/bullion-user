import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class Contact extends Component {
    componentDidMount() {
        document.title = 'Contact Us | Pawar Bullion'
        window.scrollTo(0, 0)


    }
    render() {
        return (
            <main id="content" role="main">
                <div class="bg-gray-13 bg-md-transparent">
                    <div class="container">
                        <div class="my-md-3">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><Link to="/">Home</Link></li>
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Contact Us</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="mb-8 text-center">
                        <h1>Contact Us</h1>
                    </div>


                    <div class="row mb-10">
                        <div class="col-lg-7 col-xl-6 mb-8 mb-lg-0">
                            <div class="mr-xl-6">
                                <div class="border-bottom border-color-1 mb-5">
                                    <h3 class="section-title mb-0 pb-2 font-size-25">Our Address</h3>
                                </div>

                                <address class="mb-6 text-lh-23">
                                    11, Madan Dutta Lane, Bowbazar, Kolkata-7000012
                                    <div class="">Support (033) 2237-8210</div>
                                    <div class="">Mobile +91 9007278210</div>

                                    <div class="">Email: <a class="text-blue text-decoration-on" href="mailto:pawarbullion@gmail.com">pawarbullion@gmail.com</a></div>
                                </address>
                                <h5 class="font-size-14 font-weight-bold mb-3">Opening Hours</h5>
                                <div class="">Monday to Friday: 9am-9pm</div>
                                <div class="mb-6">Saturday to Sunday: 9am-11pm</div>

                            </div>
                        </div>
                        <div class="col-lg-5 col-xl-6">
                            <div class="mb-6">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3684.2802073194734!2d88.36106971541832!3d22.56862093880319!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a027654b6ea6a87%3A0x9e9a999fb0ee444a!2sPawar%20Bullion%20Private%20Limited!5e0!3m2!1sen!2sin!4v1626339372279!5m2!1sen!2sin" width="600" height="450" style={{ border: 0 }} allowfullscreen="" loading="lazy"></iframe>
                            </div>
                            {/* <div class="border-bottom border-color-1 mb-5">
                                <h3 class="section-title mb-0 pb-2 font-size-25">Our Address</h3>
                            </div>
                   */}
                            {/* <h5 class="font-size-14 font-weight-bold mb-3">Careers</h5>
        <p class="text-gray">If you’re interested in employment opportunities at Electro, please email us: <a class="text-blue text-decoration-on" href="mailto:contact@yourstore.com">contact@yourstore.com</a></p>
     */}
                        </div>
                    </div>
                </div>

            </main>
        )
    }
}
