import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { get } from '../../utils/service'

export default class BankDetails extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             bankArr:[]
        }
    }
    
    componentDidMount() {
        document.title = 'Bank Details | Pawar Bullion'
        window.scrollTo(0,0)
        this.getBank()
    }
    getBank=()=>{
        get('/user/bankDetails/')
        .then((res) => {
            console.log(res)
            this.setState({
                bankArr: res.data
            })
        }).catch((err) => {
            console.log(err)
        })
    }
    render() {
        return (
            <main id="content" role="main">
                <div class="bg-gray-13 bg-md-transparent">
                    <div class="container">
                        <div class="my-md-3">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><Link to="/">Home</Link></li>
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Bank Details</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="mb-8 text-center">
                        <h1>Bank Details</h1>
                        </div>
                    <div class="row mb-12">
                        {
                            this.state.bankArr.map((obj,i)=>
                            <div class="col-md-4 mb-4 mb-md-0  ml-md-auto  mr-md-auto">
                            <div class="card mb-3 border-0 rounded-0">
                                <img class="img-fluid mb-3" src={obj.bankBanner} class="bankImage" name={obj.bankName} alt={obj.bankName} />
                                <div class="bankDiv">
                                        Bank Name : {obj.bankName}<br />
                                        Account Number : {obj.accountNumber}<br />
                                        Account Holder Name : {obj.accountHolderName}<br />
                                        Branch Name : {obj.branchName}<br />
                                        IFSC Code : {obj.ifscCode}<br />
                                   </div>
                            </div>
                        </div>
                            )
                        }
                        {/* <div class="col-md-4 mb-4 mb-md-0  ml-md-auto  mr-md-auto">
                            <div class="card mb-3 border-0 rounded-0">
                                <img class="img-fluid mb-3" src="/img/bank/sbi.jpg" class="bankImage" name="State Bank of India" alt="State Bank of India" />
                                <div class="bankDiv">
                                  
                                        Bank Name : State Bank of India<br />
                                        Account Number : 2147483647<br />
                                        Account Holder Name : Malay Kumar Bera<br />
                                        Branch Name : Haldia<br />
                                        IFSC Code : SBIN0015<br />
                                   </div>
                            </div>
                        </div> */}

                        {/* <div class="col-md-4 mb-4 mb-md-0  ml-md-auto  mr-md-auto">
                            <div class="card mb-3 border-0 rounded-0">
                                <img class="img-fluid mb-3" src="/img/bank/union.jpg" class="bankImage" name="State Bank of India" alt="State Bank of India" />
                                <div class="bankDiv">
                                  
                                        Bank Name : Union Bank of India<br />
                                        Account Number : 667700100006<br />
                                        Account Holder Name : Malay Kumar Bera<br />
                                        Branch Name : Haldia<br />
                                        IFSC Code : UBIN00875<br />
                                   </div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-4 mb-md-0  ml-md-auto  mr-md-auto">
                            <div class="card mb-3 border-0 rounded-0">
                                <img class="img-fluid mb-3" src="/img/bank/panjab.jpg" class="bankImage" name="State Bank of India" alt="State Bank of India" />
                                <div class="bankDiv">
                                  
                                        Bank Name : Panjab National Bank<br />
                                        Account Number : 776638733028<br />
                                        Account Holder Name : Malay Kumar Bera<br />
                                        Branch Name : Haldia<br />
                                        IFSC Code : PNB0015<br />
                                   </div>
                            </div>
                        </div>
                         */}
                        
                                           </div>

                </div>
            </main>

        )
    }
}
