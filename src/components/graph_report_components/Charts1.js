import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import ApexChart from './ApexChart';
import HighLowGraph from './HighLowGraph';
import OpenCloseHighLow from './OpenCloseHighLow';
import { get, post, withoutauthformpost } from '../../utils/service';
export default class Charts2 extends Component {
    constructor(props) {
        console.log(props)
        super(props)
        this.state = {

            name: 'Gold MCX',
            type: 'GOLDMCX',
            day: 'TODAY',
            graphData: [],
            name1: 'Gold MCX',
            name2: 'Gold MCX',
            type1: 'GOLDMCX',
            type2: 'GOLDMCX',
            day1: 'TODAY',
            graphDataHigh: [],
            graphDataLow: [],
            graphDataBuy: [],
            graphDataSell: [],
            graphDataDate: [],
            graphDataHigh1: [],
            graphDataLow1: [],
            graphDataOpen1: [],
            graphDataClose1: [],
            graphDataDate1: [],
            openClose: '0',
            day: 'TODAY',



        }
    }
    componentWillUnmount() {
        clearInterval(this.newInterval)
        console.log('timer clear')
    }
    componentDidMount() {
        document.title = 'Chart : Pawar Bullion'

        this.getGraphData(this.state.type, this.state.day)
        this.getGraphData1(this.state.type1, this.state.day1)
        this.getGraphData2(this.state.type2)
        this.newInterval = setInterval(() => {

            try {
                console.log(true)
                this.getGraphData(this.state.type, this.state.day)
                this.getGraphData1(this.state.type1, this.state.day1)
                this.getGraphData2(this.state.type2)
            }
            catch (err) {
                console.log(err)
            }
        }, 300000);
    }

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    }
    handleGraphSelectChange = (e) => {
        e.preventDefault();
        this.setState({ type: e.target.value });
        if (e.target.value == 'GOLDMCX') {
            this.setState({
                name: "Gold MCX"
            })
        } else if (e.target.value == 'SILVERMCX') {
            this.setState({
                name: "Silver MCX"
            })
        } else if (e.target.value == 'GOLD') {
            this.setState({
                name: "Gold"
            })
        } else if (e.target.value == 'SILVER') {
            this.setState({
                name: "Silver"
            })
        } else {
            this.setState({
                name: "INR"
            })
        }
        this.getGraphData(e.target.value, this.state.day)
    }
    handleHighLowGraphSelectChange = (e) => {
        e.preventDefault();
        this.setState({ type1: e.target.value });
        if (e.target.value == 'GOLDMCX') {
            this.setState({
                name1: "Gold MCX"
            })
        } else if (e.target.value == 'SILVERMCX') {
            this.setState({
                name1: "Silver MCX"
            })
        } else if (e.target.value == 'GOLD') {
            this.setState({
                name1: "Gold"
            })
        } else if (e.target.value == 'SILVER') {
            this.setState({
                name1: "Silver"
            })
        } else {
            this.setState({
                name1: "INR"
            })
        }
        this.getGraphData1(e.target.value, this.state.day1)
    }
    handleHighLowOpenCloseGraphSelectChange = (e) => {
        e.preventDefault();
        this.setState({ type2: e.target.value });
        if (e.target.value == 'GOLDMCX') {
            this.setState({
                name2: "Gold MCX"
            })
        } else if (e.target.value == 'SILVERMCX') {
            this.setState({
                name2: "Silver MCX"
            })
        } else if (e.target.value == 'GOLD') {
            this.setState({
                name2: "Gold"
            })
        } else if (e.target.value == 'SILVER') {
            this.setState({
                name2: "Silver"
            })
        } else {
            this.setState({
                name2: "INR"
            })
        }
        this.getGraphData2(e.target.value)
    }
    getGraphData2 = (type) => {
        const data = {
            'type': type
        }
        post('/statistic/graph/openClosePerDay/', data)
            .then((res) => {
                // console.log(res.data)
                this.setState({
                    graphDataHigh1: res.data.high,
                    graphDataLow1: res.data.low,
                    graphDataOpen1: res.data.open,
                    graphDataClose1: res.data.close,
                    graphDataDate1: res.data.date,
                })
            }).catch((err) => {
                console.log(err)
            })
    }
    getGraphData1 = (type, day) => {
        const data = {
            'type': type,
            'day': day
        }
        post('/statistic/graph/parDayHighLow/', data)
            .then((res) => {
                // console.log(res.data)
                this.setState({
                    graphDataHigh: res.data.high,
                    graphDataLow: res.data.low,
                    graphDataBuy: res.data.buy,
                    graphDataSell: res.data.sell,
                    graphDataDate: res.data.date,
                })
            }).catch((err) => {
                console.log(err)
            })
    }
    graphButtonClick1 = (day) => {
        this.setState({ day1: day })
        this.getGraphData1(this.state.type1, day)
        // console.log(this.state.graphData)
    }
    getGraphData = (type, day) => {
        const data = {
            'type': type,
            'day': day
        }
        post('/statistic/graph/SpecificDuration/', data)
            .then((res) => {
                // console.log(res.data)
                this.setState({
                    graphData: res.data
                })
            }).catch((err) => {
                console.log(err)
            })
    }
    graphButtonClick = (day) => {
        this.setState({ day: day })
        this.getGraphData(this.state.type, day)
        // console.log(this.state.graphData)
    }
    render() {
        return (
            <main id="content" role="main">
                <div class="bg-gray-13 bg-md-transparent">
                    <div class="container">
                        <div class="my-md-3">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><Link to="/">Home</Link></li>
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Chart</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>

                <div class="container">
                    {/* <div class="mb-8 text-center">
                        <h1>Chart</h1>
                    </div> */}
                    <div className="row">

                        <div className="col-xl-12">
                            <div className="card shadow" style={{ marginBottom: "50px", paddingRight: "15px" }}>
                                <div className="card-header bg-transparent">
                                    <div className="row align-items-center">
                                        <div className="col">
                                            <h6 className="text-uppercase  ls-1 mb-1">Day Wise</h6>
                                            <h2 className="mb-0">{this.state.name1}</h2>

                                        </div>

                                        <div>

                                            <div style={{ float: "right", marginRight: "15px" }}>
                                                {
                                                    this.state.day1 == 'TODAY' ?
                                                        <button className="btn btn-primary">Today</button>
                                                        :
                                                        <button className="btn btn-outline-primary" onClick={() => this.graphButtonClick1('TODAY')}>Today</button>
                                                }
                                                {
                                                    this.state.day1 == 'YESTERDAY' ?
                                                        <button className="btn btn-primary">Yesterday</button>
                                                        :
                                                        <button className="btn btn-outline-primary" onClick={() => this.graphButtonClick1('YESTERDAY')}>Yesterday</button>
                                                }
                                                {
                                                    this.state.day1 == '2DAYAGO' ?
                                                        <button className="btn btn-primary">2 Day Ago</button>
                                                        :
                                                        <button className="btn btn-outline-primary" onClick={() => this.graphButtonClick1('2DAYAGO')}>2 Day Ago</button>
                                                }

                                            </div>

                                        </div>
                                        <div>
                                            <select style={{ width: "200PX", cursor: "pointer", float: "right", fontSize: "18px" }} className="form-control" onChange={this.handleHighLowGraphSelectChange}>
                                                <option value="GOLDMCX">Gold MCX</option>
                                                <option value="SILVERMCX">SILVER MCX</option>
                                                <option value="GOLD">Gold</option>
                                                <option value="SILVER">SILVER</option>
                                                <option value="INR">INR</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body" style={{ height: "400px" }}>
                                    <div className={"my-pretty-chart-container chart-dropshadow h-315"}>

                                        <HighLowGraph graphDataHigh={this.state.graphDataHigh} graphDataLow={this.state.graphDataLow} graphDataBuy={this.state.graphDataBuy} graphDataSell={this.state.graphDataSell} graphDataDate={this.state.graphDataDate} />
                                        {/* <CandleStick/> */}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </main>

        )
    }
}
