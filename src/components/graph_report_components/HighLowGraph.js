import React, { Component } from 'react'
import ApexCharts from 'apexcharts'
import ReactDOM from 'react-dom';
import ReactApexChart from "react-apexcharts";
export default class HighLowGraph extends React.Component {
    constructor(props) {
      super(props);
      // console.log(props.graphData)
      this.state = {
      
        series: [],
        options: {
            xaxis: {
                type:'datetime',
                 },
            chart: {
              height: 350,
              type: 'line',
              zoom: {
                enabled: false
              }
            },
            dataLabels: {
              enabled: false
            },
            colors: ['#5b9aff', '#f6c957', '#65bd8f','#F44336'],
            stroke: {
                curve: 'smooth',
            },
            title: {
              text: 'Current Trends',
              align: 'left'
            },
            // grid: {
            //   row: {
            //     colors: ['#f3f3f3', '#f3f3f3'], // takes an array which will be repeated on columns
            //     opacity: 0.5
            //   },
            // },
          
          },
      
      
      };
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
      if (this.props !== prevProps) {
      
        //  console.log(this.props)
         this.setState({
        series: [{
            name:'Buy',
           data: this.props.graphDataBuy,
        },{
            name:'Sell',
            data: this.props.graphDataSell,
         },{
            name:'High',
            data: this.props.graphDataHigh,
         },{
            name:'Low',
            data: this.props.graphDataLow,
         }],
         options: {
            xaxis: {
                type:'datetime',
                categories: this.props.graphDataDate,
              },
         }
         })
      }
    }
  

    render() {
//         var chart = new ApexCharts(document.querySelector('#chart'), this.state.options,this.state.series)
// chart.render()
      return (
        
<>
<div id="app"></div>
  <div id="chart">
<ReactApexChart options={this.state.options} series={this.state.series} type="line" height={350} />
</div>
</>


      );
    }
  }

//   const domContainer = document.querySelector('#app');
//   ReactDOM.render(React.createElement(ApexChart), domContainer);
