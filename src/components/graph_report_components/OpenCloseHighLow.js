import React, { Component } from 'react'
import ApexCharts from 'apexcharts'
import ReactDOM from 'react-dom';
import ReactApexChart from "react-apexcharts";
export default class OpenCloseHighLow extends React.Component {
    constructor(props) {
      super(props);
      // console.log(props.graphData)
      this.state = {
          
        series: [{
          name: 'TEAM A',
          type: 'area',
          data: [23, 11, 22, 27, 13, 22, 37, 21, 44, 22, 30]
        }, {
          name: 'TEAM B',
          type: 'area',
          data: [44, 55, 41, 67, 22, 43, 21, 41, 56, 27, 43]
        }, {
          name: 'TEAM C',
          type: 'line',
          data: [30, 25, 36, 30, 45, 35, 64, 52, 59, 36, 39]
        },
        {
          name: 'TEAM D',
          type: 'line',
          data: [20, 35, 26, 20, 35, 25, 54, 42, 49, 26, 29]
        }],
        options: {
          chart: {
            height: 350,
            type: 'line',
            stacked: false,
          },
          stroke: {
            width: [0, 2, 5,5],
            curve: 'smooth'
          },
          plotOptions: {
            bar: {
              columnWidth: '50%'
            }
          },
          
          fill: {
            opacity: [0.85, 0.25, 1,1],
            gradient: {
              inverseColors: false,
              shade: 'light',
              type: "vertical",
              opacityFrom: 0.85,
              opacityTo: 0.55,
              stops: [0, 100, 100, 100]
            }
          },
          labels: [],
          markers: {
            size: 0
          },
          xaxis: {
            type: 'datetime'
          },
          yaxis: {
            title: {
              text: 'Points',
            },
            // min: 0
          },
          tooltip: {
            shared: true,
            intersect: false,
            y: {
              formatter: function (y) {
                if (typeof y !== "undefined") {
                  return y.toFixed(0) + " points";
                }
                return y;
          
              }
            }
          }
        },
      
      }
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
      if (this.props !== prevProps) {
        //  console.log(this.props)
         
        this.setState({
         series: [
          {
            name: 'Low',
            type: 'area',
            data:this.props.graphDataLow1
          },
           {
            name: 'High',
            type: 'area',
            data: this.props.graphDataHigh1
          },
           {
            name: 'Open',
            type: 'line',
            data: this.props.graphDataOpen1
          },
          {
            name: 'Close',
            type: 'line',
            data: this.props.graphDataClose1
          }
        ],
          options: {
             labels:this.props.graphDataDate1,  
         }
        })
      }
    }
  

    render() {
      // console.log(this.props.graphDataDate1)
      return (
        
<>
<div id="app"></div>
  <div id="chart">
<ReactApexChart options={this.state.options} series={this.state.series} type="line" height={350} />
</div>
</>


      );
    }
  }

//   const domContainer = document.querySelector('#app');
//   ReactDOM.render(React.createElement(ApexChart), domContainer);
