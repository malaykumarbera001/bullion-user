import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import ApexChart from './ApexChart';
import HighLowGraph from './HighLowGraph';
import OpenCloseHighLow from './OpenCloseHighLow';
import { get, post, withoutauthformpost } from '../../utils/service';
import axios from 'axios';
export default class Charts_test extends Component {
    constructor(props) {
        console.log(props)
        super(props)
        this.state = {

            name: 'Gold MCX',
            type: 'GOLDMCX',
            day: 'TODAY',
            graphData: [],
            name1: 'Gold MCX',
            name2: 'Gold MCX',
            type1: 'GOLDMCX',
            type2: 'GOLDMCX',
            day1: 'TODAY',
            graphDataHigh: [],
            graphDataLow: [],
            graphDataBuy: [],
            graphDataSell: [],
            graphDataDate: [],
            graphDataHigh1: [],
            graphDataLow1: [],
            graphDataOpen1: [],
            graphDataClose1: [],
            graphDataDate1: [],
            openClose: '0',
            day: 'TODAY',
            dataArray: []



        }
    }
    componentWillUnmount() {
        clearInterval(this.newInterval)
        console.log('timer clear')
    }
    componentDidMount() {
        document.title = 'Chart : Pawar Bullion'

        this.getGraphData()


    }

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    }

    getGraphData = () => {

        axios.get('https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=IBM&interval=60min&apikey=ZZE86EZCE44OUAKC')
            .then((res) => {
                console.log("API Data " + res.data)
                var arr = res.data['Time Series (60min)'];
                console.log(arr)
                var newArr1 = []
                for (var arr1 in arr) {
                    var newArr = {
                        'x': arr1,
                        'y': [arr[arr1]['1. open'], arr[arr1]['2. high'], arr[arr1]['3. low'], arr[arr1]['4. close']]
                    }
                    console.log(arr[arr1]['1. open'])
                    newArr1.push(newArr)
                }
                this.setState({
                    dataArray: newArr1
                })

                console.log("Time Series " + this.state.dataArray)

                // this.setState({
                //     graphData: res.data
                // })
            }).catch((err) => {
                console.log(err)
            })
    }
    graphButtonClick = (day) => {
        this.setState({ day: day })
        this.getGraphData(this.state.type, day)
        // console.log(this.state.graphData)
    }
    render() {
        return (
            <main id="content" role="main">
                <div class="bg-gray-13 bg-md-transparent">
                    <div class="container">
                        <div class="my-md-3">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><Link to="/">Home</Link></li>
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Chart</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>

                <div class="container">
                    {/* <div class="mb-8 text-center">
                        <h1>Chart</h1>
                    </div> */}
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="card shadow" style={{ marginBottom: "50px", paddingRight: "15px" }}>
                                <div className="card-header bg-transparent">
                                    <div className="row align-items-center">
                                        <div className="col">
                                            <h6 className="text-uppercase  ls-1 mb-1">Day Wise</h6>
                                            <h2 className="mb-0">{this.state.name}</h2>

                                        </div>

                                        <div>

                                            {/* <div style={{ float: "right", marginRight: "15px" }}>
                                                {
                                                    this.state.day == 'TODAY' ?
                                                        <button className="btn btn-primary">Today</button>
                                                        :
                                                        <button className="btn btn-outline-primary" onClick={() => this.graphButtonClick('TODAY')}>Today</button>
                                                }
                                                {
                                                    this.state.day == 'YESTERDAY' ?
                                                        <button className="btn btn-primary">Yesterday</button>
                                                        :
                                                        <button className="btn btn-outline-primary" onClick={() => this.graphButtonClick('YESTERDAY')}>Yesterday</button>
                                                }
                                                {
                                                    this.state.day == '2DAYAGO' ?
                                                        <button className="btn btn-primary">2 Day Ago</button>
                                                        :
                                                        <button className="btn btn-outline-primary" onClick={() => this.graphButtonClick('2DAYAGO')}>2 Day Ago</button>
                                                }

                                            </div> */}

                                        </div>
                                        {/* <div>
                                            <select style={{ width: "200PX", cursor: "pointer", float: "right", fontSize: "18px" }} className="form-control" onChange={this.handleGraphSelectChange}>
                                                <option value="GOLDMCX">Gold MCX</option>
                                                <option value="SILVERMCX">SILVER MCX</option>
                                           
                                            </select>
                                        </div> */}
                                    </div>
                                </div>
                                <div className="card-body" style={{ height: "400px" }}>
                                    <div className={"my-pretty-chart-container chart-dropshadow h-315"}>

                                        <ApexChart graphData={this.state.dataArray} />
                                        {/* <CandleStick/> */}
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>


                </div>
            </main>

        )
    }
}
