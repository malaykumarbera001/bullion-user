import React, { Component } from 'react'
import MaterialTable from 'material-table'
import { get, post } from '../../utils/service'
import { Link } from 'react-router-dom'

export default class OpenCloseReport extends Component {
    constructor(props) {
        super(props)
        var date = new Date();
        // console.log()
        this.state = {
            data: [],
            type: 'GOLDMCX',
            name: 'Gold MCX',
            month: date.getFullYear() + "-" + (date.getMonth() + 1).toString().padStart(2, 0),
            monthTitle: 'This Month'
        }
    }

    componentDidMount() {
        document.title = 'Open Close Report : Pawar Bullion'

        this.getData(this.state.type, this.state.month)
    }
    getData = (type, month) => {
        const data = {
            'type': type,
            'month': month
        }
        post('/statistic/report/openClosePerDay/', data)
            .then((res) => {
                console.log(res)
                if (res.status == 200) {
                    this.setState({
                        data: res.data
                    })
                }
            }).catch((err) => {
                console.log(err)
            })
    }
    handleChange = (e) => {
        e.preventDefault();
        this.setState({ type: e.target.value });
        if (e.target.value == 'GOLDMCX') {
            this.setState({
                name: "Gold MCX"
            })
        } else if (e.target.value == 'SILVERMCX') {
            this.setState({
                name: "Silver MCX"
            })
        } else if (e.target.value == 'GOLD') {
            this.setState({
                name: "Gold"
            })
        } else if (e.target.value == 'SILVER') {
            this.setState({
                name: "Silver"
            })
        } else {
            this.setState({
                name: "INR"
            })
        }
        this.getData(e.target.value, this.state.month)
    }
    handleMonthChange = (e) => {
        e.preventDefault();
        var m_names = ['January', 'February', 'March',
            'April', 'May', 'June', 'July',
            'August', 'September', 'October', 'November', 'December'];

        this.setState({ month: e.target.value, monthTitle: m_names[new Date(e.target.value).getMonth()] });

        this.getData(this.state.type, e.target.value)
    }
    ren
    render() {
        return (
            <main id="content" role="main">
                <div class="bg-gray-13 bg-md-transparent">
                    <div class="container">
                        <div class="my-md-3">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><Link to="/">Home</Link></li>
                                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Open Close Report</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="mb-8 text-center">
                        <h1>Open Close Report</h1>
                    </div>
                    <select className="form-control cusToSelectE" onChange={this.handleChange}>
                        <option value="GOLDMCX">GOLD MCX</option>
                        <option value="SILVERMCX">SILVER MCX</option>
                        <option value="GOLD">Gold</option>
                        <option value="SILVER">SILVER</option>
                        <option value="INR">INR</option>
                    </select>
                    <input type="month" name="month" style={{ width: "160PX", cursor: "pointer", float: "right", fontSize: "14px" }} value={this.state.month} className="form-control" onChange={this.handleMonthChange} />

                    <div className="table-responsive">

                        {/* {this.state.type} */}
                        {
                            this.state.type != 'GOLDMCX' && this.state.type != 'SILVERMCX' ?
                                <MaterialTable
                                    title={this.state.name + " Day Wize Report"}
                                    columns={[
                                        { title: 'Date', field: 'date' },
                                        { title: 'Open Bid', field: 'openBid', type: 'numeric' },
                                        { title: 'Close Bid', field: 'closeBid', type: 'numeric' },
                                        { title: 'Open Ask', field: 'openAsk', type: 'numeric' },
                                        { title: 'Close Ask', field: 'closeAsk', type: 'numeric' },
                                        { title: 'High', field: 'high', type: 'numeric' },
                                        { title: 'Low', field: 'low', type: 'numeric' },

                                    ]}
                                    data={this.state.data}
                                    options={{
                                        exportButton: true
                                    }}
                                />
                                :
                                <MaterialTable
                                    title={this.state.name + " Day Wize Report"}
                                    columns={[
                                        { title: 'Date', field: 'date' },
                                        { title: 'Open', field: 'open', type: 'numeric' },
                                        { title: 'Close', field: 'close', type: 'numeric' },
                                        { title: 'Open Bid', field: 'openBid', type: 'numeric' },
                                        { title: 'Close Bid', field: 'closeBid', type: 'numeric' },
                                        { title: 'Open Ask', field: 'openAsk', type: 'numeric' },
                                        { title: 'Close Ask', field: 'closeAsk', type: 'numeric' },
                                        { title: 'High', field: 'high', type: 'numeric' },
                                        { title: 'Low', field: 'low', type: 'numeric' },

                                    ]}
                                    data={this.state.data}
                                    options={{
                                        exportButton: true
                                    }}
                                />
                        }
                        <br />
                        <br />
                        <br />
                        <br />
                    </div>

                </div>
            </main>
        )
    }
}
