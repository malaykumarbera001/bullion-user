import React, { Component } from 'react'
import ApexCharts from 'apexcharts'
import ReactDOM from 'react-dom';
import ReactApexChart from "react-apexcharts";
export default class ApexChart extends React.Component {
    constructor(props) {
      super(props);
      // console.log(props.graphData)
      this.state = {
      
        series: [{
          data: props.graphData
        }],
        options: {
          chart: {
            type: 'candlestick',
            height: 350
          },
          title: {
            text: 'CandleStick Chart',
            align: 'left'
          },
          xaxis: {
            type: 'datetime'
          },
          yaxis: {
            tooltip: {
              enabled: true
            }
          }
        },
      
      
      };
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
      if (this.props !== prevProps) {
      
         console.log(this.props)
         this.setState({
          series: [{
            data: this.props.graphData
          }],
         })
      }
    }
  

    render() {
//         var chart = new ApexCharts(document.querySelector('#chart'), this.state.options,this.state.series)
// chart.render()
      return (
        
<>
<div id="app"></div>
  <div id="chart">
<ReactApexChart options={this.state.options} series={this.state.series} type="candlestick" height={350} />
</div>
</>


      );
    }
  }

//   const domContainer = document.querySelector('#app');
//   ReactDOM.render(React.createElement(ApexChart), domContainer);
