import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { Show_name } from '../auth_components/action';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { checkAuth } from '../../utils/auth';
// import { hashHistory } from 'react-router'
class Header extends Component {
    constructor(props) {
        super(props)

        this.state = {
            anchorEl: null,
            open: false
        }
    }

    menuClick = (path) => {
        this.props.history.push(path)
        document.getElementById('menuCloseButton').click()
        console.log('data')
    }
    handleClose = () => {
        this.setState({ anchorEl: null, open: false })
    };
    handleMenu = (event) => {
        console.log(event.currentTarget)
        this.setState({
            anchorEl: event.currentTarget,
            open: true
        })
    };
    hendleLoginMenuClick = (path) => {
        this.props.history.push(path)
        this.handleClose()
    }
    handleLogout = () => {
        checkAuth.signout();
        this.props.Profile_action();
        this.handleClose()
        localStorage.removeItem('pawarBullionUserToken');
        localStorage.removeItem('pawarBullionRefreshToken');
        localStorage.removeItem('pawarBullionPhone');
        localStorage.removeItem('pawarBullionEmail');
        localStorage.removeItem('pawarBullionName');
        localStorage.removeItem('pawarBullionId');
        this.props.history.push('/login')

    }
    render() {
        console.log(this.props)
        return (
            <header id="header" class="u-header u-header-left-aligned-nav">
                <div class="u-header__section">
                    <div class="bg-primary">
                        <div class="container">
                            <div class="row min-height-64 align-items-center position-relative">
                                <div class="col-auto" style={{ maxWidth: "182px" }}>
                                    <nav class="navbar navbar-expand u-header__navbar py-0 max-width-200 min-width-200">
                                        <Link class="order-1 order-xl-0 navbar-brand u-header__navbar-brand u-header__navbar-brand-center" to="/" aria-label="Electro">
                                            <img src="/logo/pawan bullion-01 (1).png" />
                                        </Link>
                                        <button
                                            id="sidebarHeaderInvokerMenu"
                                            type="button"
                                            class="navbar-toggler d-block btn u-hamburger mr-3 mr-xl-0 desktopHide"
                                            aria-controls="sidebarHeader"
                                            aria-haspopup="true"
                                            aria-expanded="false"
                                            data-unfold-event="click"
                                            data-unfold-hide-on-scroll="false"
                                            data-unfold-target="#sidebarHeader1"
                                            data-unfold-type="css-animation"
                                            data-unfold-animation-in="fadeInLeft"
                                            data-unfold-animation-out="fadeOutLeft"
                                            data-unfold-duration="500"
                                        >
                                            <span id="hamburgerTriggerMenu" class="u-hamburger__box">
                                                <span class="u-hamburger__inner"></span>
                                            </span>
                                        </button>
                                    </nav>
                                    <aside id="sidebarHeader1" class="u-sidebar u-sidebar--left" aria-labelledby="sidebarHeaderInvokerMenu">
                                        <div class="u-sidebar__scroller">
                                            <div class="u-sidebar__container">
                                                <div class="u-header-sidebar__footer-offset pb-0">
                                                    <div class="position-absolute top-0 right-0 z-index-2 pt-4 pr-7">
                                                        <button
                                                            id="menuCloseButton"
                                                            type="button"
                                                            class="close ml-auto "
                                                            aria-controls="sidebarHeader"
                                                            aria-haspopup="true"
                                                            aria-expanded="false"
                                                            data-unfold-event="click"
                                                            data-unfold-hide-on-scroll="false"
                                                            data-unfold-target="#sidebarHeader1"
                                                            data-unfold-type="css-animation"
                                                            data-unfold-animation-in="fadeInLeft"
                                                            data-unfold-animation-out="fadeOutLeft"
                                                            data-unfold-duration="500"
                                                        >
                                                            <span aria-hidden="true"><i class="ec ec-close-remove text-gray-90 font-size-20"></i></span>
                                                        </button>
                                                    </div>
                                                    <div class="js-scrollbar u-sidebar__body">
                                                        <div id="headerSidebarContent" class="u-sidebar__content u-header-sidebar__content">
                                                            <center>  <a class="" style={{ margin: 0 }}>
                                                                <img src="/logo/pawan bullion-01 (1).png" style={{ width: "230px" }} />

                                                            </a>
                                                            </center>
                                                            <hr style={{ margin: 0, borderColor: "#646464" }} />
                                                            <ul id="headerSidebarList" class="u-header-collapse__nav">
                                                                <li>
                                                                    <a onClick={() => this.menuClick('/')}>Live Rates</a>
                                                                </li>
                                                                <li>
                                                                    <a onClick={() => this.menuClick('/trade')}>Trade</a>
                                                                </li>
                                                                <li>
                                                                    <a onClick={() => this.menuClick('/tradeHistory')}>Trade History</a>
                                                                </li>
                                                                <li>
                                                                    <a onClick={() => this.menuClick('/pendingOrder')}>Pending Order</a>
                                                                </li>
                                                                <li>
                                                                    <a onClick={() => this.menuClick('/account')}>My Account</a>
                                                                </li>
                                                                <li>
                                                                    <a onClick={() => this.menuClick('/kycDetails')}>KYC Details</a>
                                                                </li>
                                                                {/* <li>
                                                                <a onClick={()=>this.menuClick('/account')}>My Account</a>
                                                                </li> */}
                                                                <li>
                                                                    <a onClick={() => this.menuClick('/bankDetails')}>Bank Details</a>
                                                                </li>
                                                                <li>
                                                                    <a onClick={() => this.menuClick('/message')}>Message</a>
                                                                </li>
                                                                <li>
                                                                    <a onClick={() => this.menuClick('/about')}>About Us</a>
                                                                </li>
                                                                <li>
                                                                    <a onClick={() => this.menuClick('/policy')}>Our Policy</a>
                                                                </li>
                                                                <li>
                                                                    <a onClick={() => this.menuClick('/contact')}>Contact Us</a>

                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </aside>
                                </div>
                                <div class="col d-none d-xl-block">
                                    <div class="d-none d-xl-block container">
                                        <div class="py-1">
                                            <nav class="js-mega-menu navbar navbar-expand-md u-header__navbar u-header__navbar--no-space hs-menu-initialized hs-menu-horizontal">
                                                <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
                                                    <ul class="navbar-nav u-header__navbar-nav justify-content-center">
                                                        <li class="nav-item hs-has-sub-menu u-header__nav-item"
                                                            data-event="hover"
                                                            data-animation-in="slideInUp"
                                                            data-animation-out="fadeOut">
                                                            <a id="HomeMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false" aria-labelledby="HomeSubMenu">Live Rates</a>
                                                            <ul id="HomeSubMenu" class="hs-sub-menu u-header__sub-menu animated fadeOut" aria-labelledby="HomeMegaMenu" style={{ minWidth: "230px", display: "none" }}>
                                                                <li><Link class="nav-link u-header__sub-menu-nav-link" to="/">Live Rates</Link></li>
                                                                <li><Link class="nav-link u-header__sub-menu-nav-link" to="/trade">Trade</Link></li>
                                                                <li><Link class="nav-link u-header__sub-menu-nav-link" to="/tradeHistory">Trade History</Link></li>
                                                                <li><Link class="nav-link u-header__sub-menu-nav-link" to="/pendingOrder">Pending Order</Link></li>
                                                            </ul>

                                                        </li>
                                                        {/* <li class="nav-item u-header__nav-item">
                                                            <Link class="nav-link u-header__nav-link" to="/" aria-haspopup="true" aria-expanded="false" aria-labelledby="featuresSubMenu">Live Rates</Link>
                                                        </li> */}
                                                        {/* <li class="nav-item u-header__nav-item">
                                                            <Link class="nav-link u-header__nav-link" to="/about" aria-haspopup="true" aria-expanded="false" aria-labelledby="AboutUsSubMenu">About Us</Link>
                                                        </li> */}
                                                        <li class="nav-item hs-has-sub-menu u-header__nav-item"
                                                            data-event="hover"
                                                            data-animation-in="slideInUp"
                                                            data-animation-out="fadeOut">
                                                            <a id="HomeMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false" aria-labelledby="HomeSubMenu">Booking Desk</a>
                                                            <ul id="HomeSubMenu" class="hs-sub-menu u-header__sub-menu animated fadeOut" aria-labelledby="HomeMegaMenu" style={{ minWidth: "230px", display: "none" }}>
                                                                <li><Link class="nav-link u-header__sub-menu-nav-link" to="/bankDetails">Bank Details</Link></li>
                                                                <li><Link class="nav-link u-header__sub-menu-nav-link" to="/contact">Contact Us</Link></li>
                                                            </ul>

                                                        </li>
                                                        <li class="nav-item hs-has-sub-menu u-header__nav-item"
                                                            data-event="hover"
                                                            data-animation-in="slideInUp"
                                                            data-animation-out="fadeOut">
                                                            <a id="HomeMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false" aria-labelledby="HomeSubMenu">Analytics Chart</a>
                                                            <ul id="HomeSubMenu" class="hs-sub-menu u-header__sub-menu animated fadeOut" aria-labelledby="HomeMegaMenu" style={{ minWidth: "230px", display: "none" }}>
                                                                <li><Link class="nav-link u-header__sub-menu-nav-link" to="/candleStickCharts">CandleStick Charts</Link></li>
                                                                <li><Link class="nav-link u-header__sub-menu-nav-link" to="/currentCharts">Current Trends Charts</Link></li>
                                                                <li><Link class="nav-link u-header__sub-menu-nav-link" to="/highLowCharts">Day Wise High Low Charts</Link></li>
                                                                <li><Link class="nav-link u-header__sub-menu-nav-link" to="/openCloseReport">Open Close Report</Link></li>
                                                                {/* <li><Link class="nav-link u-header__sub-menu-nav-link" to="/testCandle">Test CandleStick Chart</Link></li> */}

                                                            </ul>

                                                        </li>
                                                        {/* <li class="nav-item u-header__nav-item">
                                                            <Link class="nav-link u-header__nav-link" to="/bankDetails" aria-haspopup="true" aria-expanded="false" aria-labelledby="featuresSubMenu">Bank Details</Link>
                                                        </li> */}
                                                        <li class="nav-item u-header__nav-item">
                                                            <Link class="nav-link u-header__nav-link" to="/message" aria-haspopup="true" aria-expanded="false" aria-labelledby="featuresSubMenu">Notification</Link>
                                                        </li>

                                                        {/* <li class="nav-item u-header__nav-item">
                                                            <Link class="nav-link u-header__nav-link" to="/download" aria-haspopup="true" aria-expanded="false" aria-labelledby="featuresSubMenu">Download</Link>
                                                        </li> */}
                                                        {/* <li class="nav-item u-header__nav-item">
                                                            <Link class="nav-link u-header__nav-link" to="/policy" aria-haspopup="true" aria-expanded="false" aria-labelledby="featuresSubMenu">Policy</Link>
                                                        </li> */}
                                                        <li class="nav-item u-header__nav-item">
                                                            <Link class="nav-link u-header__nav-link" to="/contact" aria-haspopup="true" aria-expanded="false">Contact Us</Link>
                                                        </li>

                                                    </ul>
                                                </div>
                                            </nav>
                                        </div>
                                    </div>

                                </div>
                                <div class="col col-xl-auto text-right text-xl-left pl-0 pl-xl-3 position-static">
                                    <div class="d-inline-flex">
                                        <ul class="d-flex list-unstyled mb-0 align-items-center">
                                            <li class="col d-none d-xl-block">
                                                <a href="tel:+919765787698" class="text-gray-90 myClassA" data-toggle="tooltip" data-placement="top" title="Compare"><i class="font-size-22 ec ec-support"></i></a>
                                            </li>
                                            <li class="col d-none d-xl-block">
                                                <a href="whatsapp://send?phone=916294008510" class="text-gray-90 myClassA" data-toggle="tooltip" data-placement="top" title="Favorites"><i class="font-size-22 fab fa-whatsapp"></i></a>
                                            </li>

                                            <li class="col pr-xl-0 px-2 px-sm-3">
                                                {/* {String(this.props.profile_details)} */}
                                                {
                                                    this.props.profile_details == true ?
                                                        String(localStorage.getItem('pawarBullionUserToken')).length > 10 ?
                                                            <>
                                                                <span class="text-gray-90 position-relative newSpan d-flex" data-toggle="tooltip" data-placement="top" title="My Account" onClick={this.handleMenu}>
                                                                    <i class="font-size-26 ec ec-user"></i>
                                                                    <span class="d-none d-xl-block font-weight-bold font-size-16 text-gray-90" style={{ marginLeft: "0.3rem" }}>{String(localStorage.getItem('pawarBullionName')).split(' ').slice(0, 1)}</span>
                                                                </span>
                                                                <Menu
                                                                    id="menu-appbar"
                                                                    anchorEl={this.state.anchorEl}
                                                                    anchorOrigin={{
                                                                        vertical: 'top',
                                                                        horizontal: 'right',
                                                                    }}
                                                                    keepMounted
                                                                    transformOrigin={{
                                                                        vertical: 'top',
                                                                        horizontal: 'right',
                                                                    }}
                                                                    open={this.state.open}
                                                                    onClose={this.handleClose}
                                                                >
                                                                    <MenuItem onClick={() => this.hendleLoginMenuClick('/account')}>My account</MenuItem>
                                                                    <MenuItem onClick={() => this.hendleLoginMenuClick('/trade')}>Trade</MenuItem>
                                                                    <MenuItem onClick={() => this.hendleLoginMenuClick('/tradeHistory')}>Trade History</MenuItem>
                                                                    <MenuItem onClick={() => this.hendleLoginMenuClick('/pendingOrder')}>Pending Order</MenuItem>
                                                                    <MenuItem onClick={() => this.hendleLoginMenuClick('/kycDetails')}>KYC Details</MenuItem>
                                                                    <MenuItem onClick={this.handleLogout}>Logout</MenuItem>

                                                                </Menu>



                                                            </> :
                                                            <Link to="/login" class="text-gray-90 position-relative d-flex" data-toggle="tooltip" data-placement="top" title="Login">
                                                                <i class="font-size-26 ec ec-user"></i>
                                                                <span class="d-none d-xl-block font-weight-bold font-size-16 text-gray-90" style={{ marginLeft: "0.3rem" }}>Login</span>
                                                            </Link> :
                                                        <Link to="/login" class="text-gray-90 position-relative d-flex" data-toggle="tooltip" data-placement="top" title="Login">
                                                            <i class="font-size-26 ec ec-user"></i>
                                                            <span class="d-none d-xl-block font-weight-bold font-size-16 text-gray-90" style={{ marginLeft: "0.3rem" }}>Login</span>
                                                        </Link>
                                                }
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </header>

        )
    }
}

const mapStateToProps = (state) => {
    // console.log(state.profile);


    return {
        profile_details: state.profile,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        Profile_action: () => { dispatch(Show_name()) },

    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Header);


