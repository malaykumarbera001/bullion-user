import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class Footer extends Component {
    render() {
        return (
            <footer>
             <div class="bg-primary py-3">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="row align-items-center">
                                <div class="col-auto flex-horizontal-center col-md-12">
                                   
                                   <center class="col-md-12"> <h2 class="font-size-20 mb-0 ml-3" style={{color:"#fed700"}}>Pawar Bullion</h2></center>
                                </div>
                               
                            </div>
                        </div>
                       </div>
                </div>
            </div>
            <div class="pt-8 pb-4 bg-gray-13">
                <div class="container mt-1">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="mb-6">
                                <center><a href="#" class="d-inline-block">
                                <img src="/logo/pawan bullion-01 (1).png"  style={{width:"67%"}}/>    </a><br/>
                              <strong>Download Our apps from</strong><br/>
                              <br/>
                                
                                <img src="/icon/app/android.png"  style={{width:"40%",marginRight:"10px"}}/>
                                <img src="/icon/app/Apple-App-Store-Icon.png"  style={{width:"40%",marginLeft:"10px"}}/>
                                </center>
                           
                            </div>
                      
                           
                            </div>
                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-12 col-md mb-3 mb-md-0">
                                    <h6 class="mb-3 font-weight-bold">Find it Fast</h6>
                                    <ul class="list-group list-group-flush list-group-borderless mb-0 list-group-transparent">
                                        <li><Link class="list-group-item list-group-item-action" to="/">Live Rates</Link></li>
                                        <li><Link class="list-group-item list-group-item-action" to="about">About Us</Link></li>
                                        <li><Link class="list-group-item list-group-item-action" to="/bankDetails">Bank Details</Link></li>
                                        <li><Link class="list-group-item list-group-item-action" to="/message">Message</Link></li>
                                        <li><Link class="list-group-item list-group-item-action" to="/policy">Our Policy</Link></li>
                                        <li><Link class="list-group-item list-group-item-action" to="/contact">Contact Us</Link></li>
                                    </ul>
                                </div>
                                <div class="col-12 col-md mb-4 mb-md-0">
                                     <ul class="list-group list-group-flush list-group-borderless mb-0 list-group-transparent">
                                        <li><Link class="list-group-item list-group-item-action" to="/trade">Trade</Link></li>
                                        <li><Link class="list-group-item list-group-item-action" to="/tradeHistory">Trade History</Link></li>
                                        <li><Link class="list-group-item list-group-item-action" to="/pendingOrder">Pending Order</Link></li>
                                        <li><Link class="list-group-item list-group-item-action" to="/account">My Account</Link></li>
                                        <li><Link class="list-group-item list-group-item-action" to="/kycDetails">KYC Details</Link></li>
                                       
                                          </ul>
                                </div>
        
                                </div>
                                </div>
                        <div class="col-lg-4">
        
                                <div class="col-12 col-md mb-5 mb-md-0">
                                <div class="mb-4">
                                <div class="row no-gutters">
                                    <div class="col-auto">
                                        <i class="ec ec-support text-primary font-size-56"></i>
                                    </div>
                                    <div class="col pl-3">
                                        <div class="font-size-13 font-weight-light">Got questions? Call us 24/7!</div>
                                        <a href="tel:+80080018588" class="font-size-20 text-gray-90" style={{color:"rgb(254, 215, 0)"}}>(033) 2237-8210</a>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-4">
                                <h6 class="mb-1 font-weight-bold">Contact info</h6>
                                <address class="">
                                    11, Madan Dutta Lane, Bowbazar, Kolkata-7000012
                                </address>
                            </div>
                            <div class="my-4 my-md-4">
                                <ul class="list-inline mb-0 opacity-7">
                                    <li class="list-inline-item mr-0">
                                        <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                                            <span class="fab fa-facebook-f btn-icon__inner"></span>
                                        </a>
                                    </li>
                                    <li class="list-inline-item mr-0">
                                        <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                                            <span class="fab fa-google btn-icon__inner"></span>
                                        </a>
                                    </li>
                                    <li class="list-inline-item mr-0">
                                        <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                                            <span class="fab fa-twitter btn-icon__inner"></span>
                                        </a>
                                    </li>
                                    <li class="list-inline-item mr-0">
                                        <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                                            <span class="fab fa-github btn-icon__inner"></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                     
                                </div>
                            </div>
                       
                    </div>
                </div>
            </div>
        
            <div class="bg-gray-14 py-2">
                <div class="container">
                    <div class="flex-center-between d-block d-md-flex">
                        <div class="mb-3 mb-md-0">2021 © <a href="#" class="font-weight-bold text-gray">Pawar Bullion</a> - All rights Reserved</div>
                        <div class="text-md-right">
                        Powered by Approch India
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        
        )
    }
}
