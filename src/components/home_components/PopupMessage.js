import React, { Component } from 'react'
import { Modal } from 'react-responsive-modal';
import { Link } from 'react-router-dom';
import { get } from '../../utils/service';

export default class PopupMessage extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
           
                open: false,
                message:''
        
        }
    }
    componentDidMount(){
        this.getMessage()
    }
    onCloseModal = () => {
       
        this.setState({
            open: false,
        })
    }
    getMessage=()=>{
        get('/user/message/')
        .then((res) => {
            console.log(res.data.filter(word => word.page =="Home")[0].message)
            this.setState({
                open:true,
                message: res.data.filter(word => word.page =="Home")[0].message
            })
        }).catch((err) => {
            console.log(err)
        })
    }
    render() {
        return (
            <div>
                <Modal closeOnEsc={false} closeOnOverlayClick={false} center open={this.state.open} onClose={this.onCloseModal}
                    classNames={{
                        modal: 'customModalNews',
                    }}
                >
                    <div className="row">
                      <div className="messagePopup"  dangerouslySetInnerHTML={{ __html: this.state.message}}></div>
                    </div>

                </Modal>
            </div>
        )
    }
}
