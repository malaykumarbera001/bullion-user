import { Link } from 'react-router-dom'
import React, { Component } from 'react'
import { authpost, get, post, withoutauthformpost } from '../../utils/service'
import firebase from '../../utils/firebase'
import { connect } from 'react-redux';
import { Show_name } from '../auth_components/action';
import { ToastContainer, toast } from 'react-toastify';
import { checkAuth } from '../../utils/auth';
import Moment from 'moment-js';

var urlCrypt = require('url-crypt')('~{ry*I)==yU/]9<7DPk!Hj"R#:-/Z7(hTBnlRS=4CXF');
class MainHome extends Component {
    constructor(props) {
        super(props)

        this.state = {
            goldMCX: [],
            silverMCX: [],
            gold: [],
            silver: [],
            INR: [],
            goldMCXBidColor: 'eq',
            silverMCXBidColor: 'eq',
            goldColor: 'eq',
            silverColor: 'eq',
            INRColor: 'eq',
            goldMCXAskColor: 'eq',
            silverMCXAskColor: 'eq',
            index: [],
            marquee: [],
            marquee1: [],
            FNOGOLD: [],
            FNOSILVER: [],

        }
    }

    componentDidMount() {
        document.title = 'Live Rate | Pawar Bullion'
        window.scrollTo(0, 0)
        this.getLiveRate()
        this.getIndex()
        this.getMarquee()
        this.getLiveFNO()
        setTimeout(() => { this.checkAuth() }, 2000);

    }
    checkAuth = () => {
        // console.log(localStorage.getItem('pawarBullionRefreshToken'))
        if (String(localStorage.getItem('pawarBullionName')).length > 0) {
            authpost('/auth/authCheck/')
                .then((res) => {
                    if (res.status == 200) {
                        console.log("Login Found")
                    } else {
                        const data = {
                            'refresh': localStorage.getItem('pawarBullionRefreshToken')
                        }
                        post('/auth/refresh/', data)
                            .then((res) => {
                                // console.log(res.data.access)
                                if (res.status == 200) {
                                    localStorage.setItem('pawarBullionUserToken', res.data.access);
                                    console.log("Login Renew")
                                    this.props.Profile_action();

                                } else {
                                    console.log("Login Expaire")
                                    toast.dark('Session Expaire. Plaese Login....', {
                                        position: "bottom-center",
                                        autoClose: 5000,
                                        hideProgressBar: false,
                                        closeOnClick: true,
                                        pauseOnHover: true,
                                        draggable: true,
                                        progress: undefined,
                                    });
                                    checkAuth.signout();
                                    this.props.Profile_action();
                                    localStorage.removeItem('pawarBullionUserToken');
                                    localStorage.removeItem('pawarBullionRefreshToken');
                                    localStorage.removeItem('pawarBullionPhone');
                                    localStorage.removeItem('pawarBullionEmail');
                                    localStorage.removeItem('pawarBullionName');
                                    localStorage.removeItem('pawarBullionId');
                                }
                            }).catch((err) => {
                                console.log(err)
                            })
                    }
                }).catch((err) => {
                    console.log(err)
                })
        } else {
            console.log("No Login")
        }
    }
    getMarquee = () => {
        get('/user/notification/')
            .then((res) => {
                // console.log(res)
                this.setState({
                    marquee: res.data.filter(word => word.page == 'Home Top'),
                    marquee1: res.data.filter(word => word.page == 'Home Bottom')

                })
            }).catch((err) => {
                console.log(err)
            })
    }
    getIndex = () => {
        get('/user/index/')
            .then((res) => {
                // console.log(res)
                this.setState({
                    index: res.data
                })
            }).catch((err) => {
                console.log(err)
            })
    }
    getLiveFNO = () => {
        var db = firebase.firestore();
        db.collection("LiveRateNewAPI").doc("0xXSwJrpwR4VPFBcMvlu")
            .onSnapshot((doc) => {
                var todos = [];
                todos = doc.data()['rows']
                console.log(todos)
                // console.log(todos.length)
                var GOLDArr = []
                var SILVERArr = []

                for (let property in todos) {
                    console.log(`${property}: ${todos[property]}`);
                    if (todos[property].symbol == 'GOLD') {
                        var GOLDArr1 = {
                            'month': property,
                            'data': todos[property]
                        }
                        GOLDArr.push(GOLDArr1)
                    } else {
                        var SILVERArr1 = {
                            'month': property,
                            'data': todos[property]
                        }
                        SILVERArr.push(SILVERArr1)

                    }

                }
                this.setState({
                    FNOGOLD: GOLDArr,
                    FNOSILVER: SILVERArr
                })

                // todos.for(element => {
                //     console.log(element)

                // });
                // console.log(todos.filter(word => word.symbol == 'GOLD'))
                // this.setState({
                //     FNOGOLD: todos.filter(word => word.symbol == 'GOLD'),
                //     FNOSILVER: todos.filter(word => word.symbol == 'SILVER')

                // })
            });

    }
    getLiveRate = () => {

        var db = firebase.firestore();



        db.collection("LiveRateNewAPI").doc("DGxd1BbSKTzGML1cvOB3")
            .onSnapshot((doc) => {
                var today = new Date();
                var time = today.getSeconds() + ":" + today.getMilliseconds();

                console.log("Current data at : " + time, doc.data());
                var todos = doc.data()

                // this.setState({
                //     goldMCXBidColor: 'eq',
                //     silverMCXBidColor: 'eq',
                //     goldColor: 'eq',
                //     silverColor: 'eq',
                //     INRColor: 'eq',
                //     goldMCXAskColor: 'eq',
                //     silverMCXAskColor: 'eq',
                // })
                var goldMCXAskOld = this.state.goldMCX['sell']
                if (goldMCXAskOld > todos['rows']['GOLD']['sell']) {
                    this.setState({
                        goldMCXAskColor: 'down'
                    })
                } else if (goldMCXAskOld < todos['rows']['GOLD']['sell']) {
                    this.setState({
                        goldMCXAskColor: 'up'
                    })
                } else {
                    this.setState({
                        goldMCXAskColor: 'eq'
                    })
                }
                var goldMCXBidOld = this.state.goldMCX['buy']
                if (goldMCXBidOld > todos['rows']['GOLD']['buy']) {
                    this.setState({
                        goldMCXBidColor: 'down'
                    })
                } else if (goldMCXBidOld < todos['rows']['GOLD']['buy']) {
                    this.setState({
                        goldMCXBidColor: 'up'
                    })
                } else {
                    this.setState({
                        goldMCXBidColor: 'eq'
                    })
                }
                var silverMCXAskOld = this.state.silverMCX['sell']
                if (silverMCXAskOld > todos['rows']['SILVER']['sell']) {
                    this.setState({
                        silverMCXAskColor: 'down'
                    })
                } else if (silverMCXAskOld < todos['rows']['SILVER']['sell']) {
                    this.setState({
                        silverMCXAskColor: 'up'
                    })
                } else {
                    this.setState({
                        silverMCXAskColor: 'eq'
                    })
                }
                var silverMCXBidOld = this.state.silverMCX['buy']
                if (silverMCXBidOld > todos['rows']['SILVER']['buy']) {
                    this.setState({
                        silverMCXBidColor: 'down'
                    })
                } else if (silverMCXBidOld < todos['rows']['SILVER']['buy']) {
                    this.setState({
                        silverMCXBidColor: 'up'
                    })
                } else {
                    this.setState({
                        silverMCXBidColor: 'eq'
                    })
                }
                try {
                    this.setState({
                        goldMCX: todos['rows']['GOLD'],
                        silverMCX: todos['rows']['SILVER'],
                        gold: todos['rows']['XAUUSD'],
                        silver: todos['rows']['XAGUSD'],
                        INR: todos['rows']['INRSpot'],
                        goldColor: todos['rows']['XAUUSD']['BidStatus'],
                        silverColor: todos['rows']['XAGUSD']['BidStatus'],
                        INRColor: todos['rows']['INRSpot']['BidStatus'],

                        // goldMCXAskColor: todos['rows'][0]['AskStatus'],
                        // silverMCXAskColor: todos['rows'][1]['AskStatus'],
                        // goldMCXBidColor: todos['rows'][0]['BidStatus'],
                        // silverMCXBidColor: todos['rows'][1]['BidStatus'],
                    })
                }
                catch (err) {
                    console.log(err.message)
                }
                // console.log(this.state.silverMCXAskColor)
            });




        // const data = firebase.database().ref();
        // console.log(data)
        // data.on('value', (snapshot) => {
        //     const todos = snapshot.val();
        //     console.log(todos)
        //     this.setState({
        //         goldMCXBidColor: 'eq',
        //         silverMCXBidColor: 'eq',
        //         goldColor: 'eq',
        //         silverColor: 'eq',
        //         INRColor: 'eq',
        //         goldMCXAskColor: 'eq',
        //         silverMCXAskColor: 'eq',
        //     })
        //     this.setState({
        //         goldMCX: todos['rows'][0],
        //         silverMCX: todos['rows'][1],
        //         gold: todos['rows'][2],
        //         silver: todos['rows'][3],
        //         INR: todos['rows'][4],
        //         goldMCXBidColor: todos['rows'][0]['BidStatus'],
        //         silverMCXBidColor: todos['rows'][1]['BidStatus'],
        //         goldColor: todos['rows'][2]['BidStatus'],
        //         silverColor: todos['rows'][3]['BidStatus'],
        //         INRColor: todos['rows'][4]['BidStatus'],
        //         goldMCXAskColor: todos['rows'][0]['AskStatus'],
        //         silverMCXAskColor: todos['rows'][1]['as'],
        //     })
        // });
    }
    render() {
        return (
            <div>
                {/* style={{ backgroundImage: "url(/img/banner/photo-1520531158340-44015069e78e.jpg" }} */}
                <div class="bg-img-hero backgroundImage">
                    <div class="container">
                        <div class="row" style={{ paddingTop: "20px" }}>

                            <div class="marqueeDivMar col-md-12 mb-4 mb-md-0 " style={{ padding: 0 }}>
                                <marquee scrollamount="20">
                                    {
                                        this.state.marquee.map((obj, i) =>
                                            <>

                                                <div class="customH1 basvamn bgtyd">
                                                    {obj.message}
                                                </div>
                                            </>
                                        )
                                    }
                                </marquee>
                            </div>
                        </div>
                        <div class="flex-content-center max-width-620-lg flex-column mx-auto text-center min-height-564">

                            <div class="col-md-12 dnadnA">
                                <div class="customH1 font-weight-bold">Live Rates</div>
                                <div class="row">
                                    <div class=" col-4" style={{ padding: "0 10px 0 0" }}>
                                        <div class="card shadow text-center customeDiv">
                                            <div class="text mt-2 ">
                                                <span class="mb-0 Rates customH4 customRow custoBackground"><span className={this.state.goldColor}>{this.state.gold['buy']}</span></span>
                                                <label class="text-truncate font-weight-bold" style={{ margin: 0 }}>GOLD</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=" col-4" style={{ padding: "0 10px 0 0" }}>
                                        <div class="card shadow text-center customeDiv">
                                            <div class="text mt-2 ">
                                                <span class="mb-0 Rates customH4 customRow custoBackground"><span className={this.state.silverColor}>{this.state.silver['buy']}</span></span>
                                                <label class="text-truncate font-weight-bold" style={{ margin: 0 }}>SILVER</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=" col-4" style={{ padding: "0 10px 0 0" }}>
                                        <div class="card shadow text-center customeDiv">
                                            <div class="text mt-2 ">
                                                <span class="mb-0 Rates customH4 customRow custoBackground"><span className={this.state.INRColor}>{this.state.INR['buy']}</span></span>
                                                <label class="text-truncate font-weight-bold" style={{ margin: 0 }}>INR</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row" style={{ marginRight: "-5px" }}>
                                    <div className="col-12" style={{ padding: 0 }}>
                                        <div className="row nagetiveMargin" style={{ margin: 0 }}>
                                            <div class="col-6 align-middle"></div>
                                            <div class="col-3"><span class="text-truncate font-weight-bold">Buy</span></div>
                                            <div class="col-3"><span class="text-truncate font-weight-bold">Sell</span></div>
                                        </div>
                                        <div className="row trDff1 customeDiv">
                                            <div class="col-6 align-middle marginTop12"><div class="customH5">GOLD MCX</div></div>
                                            <div class="col-3">
                                                <span className="Rates customH4 customRow custoBackground" style={{ margin: 0 }}><span className={this.state.goldMCXBidColor}>{this.state.goldMCX['buy']}</span></span>
                                                <span>High : {this.state.goldMCX['high']}</span>
                                            </div>
                                            <div class="col-3">
                                                <span className="Rates customH4 customRow custoBackground" style={{ margin: 0 }}><span className={this.state.goldMCXAskColor}>{this.state.goldMCX['sell']}</span></span>
                                                <span>Low : {this.state.goldMCX['low']}</span>
                                            </div>
                                        </div>
                                        <div className="row trDff1 customeDiv">
                                            <div class="col-6 align-middle marginTop12"><div class="customH5">SILVER MCX</div></div>
                                            <div class="col-3">
                                                <span className="Rates customH4 customRow custoBackground" style={{ margin: 0 }}><span className={this.state.silverMCXBidColor}>{this.state.silverMCX['buy']}</span></span>
                                                <span>High : {this.state.silverMCX['high']}</span>
                                            </div>
                                            <div class="col-3">
                                                <span className="Rates customH4 customRow custoBackground" style={{ margin: 0 }}><span className={this.state.silverMCXAskColor}>{this.state.silverMCX['sell']}</span></span>
                                                <span>Low : {this.state.silverMCX['low']}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row" style={{ marginRight: "-5px" }}>
                                    <div className="col-12" style={{ padding: 0 }}>
                                        <div className="row nagetiveMargin" style={{ margin: 0 }}>
                                            <div class="col-6 align-middle"></div>
                                            <div class="col-3"><span class="text-truncate font-weight-bold">Buy</span></div>
                                            <div class="col-3"><span class="text-truncate font-weight-bold">Sell</span></div>
                                        </div>
                                        {
                                            this.state.index.map((obj, i) =>

                                                obj.clickble ?

                                                    <Link to={"/buy?NhTF=" + urlCrypt.cryptObj(String(obj.addAskAmount)) +
                                                        "&Htdf=" + urlCrypt.cryptObj(String(obj.addBidAmount)) +
                                                        "&GfT=" + urlCrypt.cryptObj(String(obj.id)) +
                                                        "&NmlJhTF=" + urlCrypt.cryptObj(String(obj.title))

                                                    } className="linkClass">
                                                        <div className="row trDff customeDiv divDis">
                                                            <div class="col-6 align-middle" style={{ display: "table" }}><div class="customH6 hindadsa" style={{ display: "table-cell", verticalAlign: "middle" }}>{obj.title}
                                                                {/* {
                                                                    obj.discount != 0 ?
                                                                        <>
                                                                            <br /><span style={{ fontSize: "13px" }}>(Discount : &#8377;{obj.discount})</span>
                                                                        </> : null
                                                                } */}


                                                            </div></div>
                                                            <div class="col-3">
                                                                <span className="Rates customH4 customRow custoBackground" style={{ margin: 0 }}><span className={this.state.goldMCXBidColor}>{parseInt(this.state.goldMCX['buy']) + parseInt(obj.addBidAmount)}</span></span>
                                                                <span>High : {parseInt(this.state.goldMCX['high']) + parseInt(obj.addBidAmount)}</span>
                                                            </div>
                                                            <div class="col-3">
                                                                <span className="Rates customH4 customRow custoBackground" style={{ margin: 0 }}><span className={this.state.goldMCXAskColor}>{parseInt(this.state.goldMCX['sell']) + parseInt(obj.addAskAmount)}</span></span>
                                                                <span>Low : {parseInt(this.state.goldMCX['low']) + parseInt(obj.addAskAmount)}</span>
                                                            </div>
                                                        </div>
                                                    </Link>
                                                    :
                                                    <div className="row trDff1 customeDiv">
                                                        <div class="col-6 align-middle" style={{ display: "table" }}><div class="customH6" style={{ display: "table-cell", verticalAlign: "middle" }}>{obj.title}</div></div>
                                                        <div class="col-3">
                                                            <span className="Rates customH4 customRow custoBackground" style={{ margin: 0 }}><span className={this.state.goldMCXBidColor}>{parseInt(this.state.goldMCX['buy']) + parseInt(obj.addBidAmount)}</span></span>
                                                            <span>High : {parseInt(this.state.goldMCX['high']) + parseInt(obj.addBidAmount)}</span>
                                                        </div>
                                                        <div class="col-3">
                                                            <span className="Rates customH4 customRow custoBackground" style={{ margin: 0 }}><span className={this.state.goldMCXAskColor}>{parseInt(this.state.goldMCX['sell']) + parseInt(obj.addAskAmount)}</span></span>
                                                            <span>Low : {parseInt(this.state.goldMCX['low']) + parseInt(obj.addAskAmount)}</span>
                                                        </div>
                                                    </div>


                                            )
                                        }
                                    </div>
                                </div>
                                <div className="row" style={{ marginRight: "-5px" }}>
                                    <div className="col-12" style={{ padding: 0 }}>
                                        <div className="row nagetiveMargin" style={{ margin: 0 }}>

                                            <div class="col-12" style={{ textAlign: "left", padding: 0 }}><span class="text-truncate font-weight-bold customFontSizre">FNO GOLD</span></div>

                                        </div>
                                        <div className="row trDff1 customeDiv">

                                            {
                                                this.state.FNOGOLD.map((obj, i) =>
                                                    i < 4 ?
                                                        <div class="col-3">
                                                            <span style={{ fontWeight: "600" }}>{obj.month}</span>

                                                            <span className="Rates customH4 customRow custoBackground" style={{ margin: 0 }}>
                                                                {
                                                                    obj.data.net_change > 0 ?
                                                                        <span className='up'>{obj.data.last_price}</span>
                                                                        : obj.data.net_change == 0 ?
                                                                            <span className='eq'>{obj.data.last_price}</span>
                                                                            :
                                                                            <span className='down'>{obj.data.last_price}</span>

                                                                }
                                                            </span>
                                                            <span className="netChange customRow" style={{ fontWeight: "100" }}>{obj.data.close}
                                                                {
                                                                    obj.data.net_change > 0 ?
                                                                        <span style={{ fontWeight: "100" }}>(+{obj.data.net_change})</span>
                                                                        : obj.data.net_change < 0 ?
                                                                            <span style={{ fontWeight: "100" }}>({obj.data.net_change})</span>
                                                                            : null
                                                                }

                                                            </span>

                                                        </div>
                                                        : null
                                                )
                                            }

                                        </div>
                                        <div className="row nagetiveMargin" style={{ margin: 0 }}>

                                            <div class="col-12" style={{ textAlign: "left", padding: 0 }}><span class="text-truncate font-weight-bold customFontSizre">FNO SILVER</span></div>

                                        </div>
                                        <div className="row trDff1 customeDiv">
                                            {
                                                this.state.FNOSILVER.map((obj, i) =>
                                                    i < 4 ?
                                                        <div class="col-3">
                                                            <span style={{ fontWeight: "600" }}>{obj.month}</span>

                                                            <span className="Rates customH4 customRow custoBackground" style={{ margin: 0 }}>
                                                                {
                                                                    obj.data.net_change > 0 ?
                                                                        <span className='up' >{obj.data.last_price}</span>
                                                                        : obj.data.net_change == 0 ?
                                                                            <span className='eq'>{obj.data.last_price}</span>
                                                                            :
                                                                            <span className='down'>{obj.data.last_price}</span>

                                                                }
                                                            </span>
                                                            <span className="netChange customRow" style={{ fontWeight: "100" }}>{obj.data.close}
                                                                {
                                                                    obj.data.net_change > 0 ?
                                                                        <span style={{ fontWeight: "100" }}>(+{obj.data.net_change})</span>
                                                                        : obj.data.net_change < 0 ?
                                                                            <span style={{ fontWeight: "100" }}>({obj.data.net_change})</span>
                                                                            : null
                                                                }

                                                            </span>

                                                        </div>
                                                        : null
                                                )
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container ">
                    <div class="row marqueeDiv">
                        <div class="col-md-12 mb-4 mb-md-0">
                            <marquee scrollamount="20" class="marqueeDivMar">
                                {
                                    this.state.marquee1.map((obj, i) =>
                                        <>

                                            <div class="customH1 basvamn bgtyd">
                                                {obj.message}
                                            </div>
                                        </>
                                    )
                                }


                            </marquee>
                        </div>
                    </div>
                </div>
                <ToastContainer />

            </div>
        )
    }
}
const mapStateToProps = (state) => {
    // console.log(state.profile);


    return {
        profile_details: state.profile,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        Profile_action: () => { dispatch(Show_name()) },

    }
}


export default connect(mapStateToProps, mapDispatchToProps)(MainHome);
