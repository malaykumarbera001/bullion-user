import axios from 'axios';
import {checkAuth} from '../../utils/auth';
import {authget,get,post,authpost,authformpost} from '../../utils/service'
import * as CONSTANT from '../../utils/constant';


export const Show_name=()=>{
    return (dispatch)=>{
        authpost('/auth/authCheck/')
        .then((res) => {
            if(res.status==200){        
                dispatch({type:'SHOW_NAME',payload:true})
            }else{         
                dispatch({type:'SHOW_NAME',payload:false})
            }
        }).catch((err) => {
            console.log(err)
        })
    }

}
