import React, { Component } from 'react'
import { BsChevronRight } from "react-icons/bs";
import { Link } from 'react-router-dom'
import { ToastContainer, toast } from 'react-toastify';
import { post, withoutauthformpost } from '../../utils/service';
import { checkAuth } from '../../utils/auth';
import {connect} from 'react-redux';
import {Show_name} from './action';
import 'react-toastify/dist/ReactToastify.css';
class Register extends Component {
    constructor(props) {
        super(props)

        this.state = {
            otp: 0,
            phone: '',
            ist: 0,
            sec: 0,
            third: 0,
            fourth: 0,
            fifth: 0,
            sixth: 0,
            email: '',
            name: '',
            loading:false,
            otpSendStatus:false,
        }
    }

    componentDidMount() {
        document.title = 'Register | Pawar Bullion'
        window.scrollTo(0, 0)

    }
    handleClick = () => {
        console.log(String(this.state.name).length)
        var mailformat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        console.log(this.state.email.match(mailformat))
        if (String(this.state.name).length == 0) {
            toast.dark('Please enter your name', {
                position: "bottom-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        } else if (String(this.state.phone).length != 10) {
            toast.dark('Please enter valid phone number', {
                position: "bottom-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        } else if (this.state.email.match(mailformat)==null){
            toast.dark('Please enter your email', {
                position: "bottom-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        } else {
            this.setState({
                loading:true
            })
            const data={
                'phone':this.state.phone
            }
            post('/auth/userCheckPhone/',data)
            .then((res) => {
                console.log(res.data)
                if(res.data.status){
                    toast.dark('Phone number already reagister. Please Login..', {
                        position: "bottom-center",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    this.setState({
                        loading:false
                    })
                }else{
                    this.sendOTP(0)
                }
            }).catch((err) => {
                console.log(err)
            })
        }

    }
    sendOTP=(type)=>{
        var otp=1234
        this.setState({
            otp: otp
        })
        const data={
            'phone':this.state.phone,
            'msg':'Please use this OTP '+otp+' for complete the authentication process of Pawar Bullion',
            'template_id':''

        }
        post('/auth/sendSMS/',data)
        .then((res) => {
            this.setState({
                otpSendStatus:true,
                loading:false
            })
            if(type==1){
                toast.dark('OTP send sucessfully on your mobile number', {
                    position: "bottom-center",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            }
        }).catch((err) => {
            console.log(err)
        })
    }
    handleEdit = () => {
        this.setState({
            otp: 0,
            otpSendStatus:false
        })
    }
    handleLogin = () => {
        let newOtp = String(this.state.ist) + String(this.state.sec) + String(this.state.third) + String(this.state.fourth)
        console.log(newOtp)
        if(this.state.otp==newOtp){
            this.setState({
                loading:true
            })
            var formData = new FormData()
            formData.append('phone',this.state.phone)
            formData.append('name',this.state.name)
            formData.append('email',this.state.email)

            withoutauthformpost('/auth/userRegister/',formData)
            .then((res) => {
                const logindata={
                    'phone':this.state.phone,
                }
                post('/auth/userOTPLogin/',logindata)
                .then((data) => {
                    checkAuth.authenticate();
                    var token = JSON.parse(data.data.tokens.replace(/'/g, '"'));
                    localStorage.setItem('pawarBullionUserToken', token.access);
                    localStorage.setItem('pawarBullionRefreshToken', token.refresh);
                    localStorage.setItem('pawarBullionPhone', data.data.phone);
                    localStorage.setItem('pawarBullionEmail', data.data.email);
                    localStorage.setItem('pawarBullionName', data.data.name);
                    localStorage.setItem('pawarBullionId', data.data.id);
                    this.props.Profile_action();
                    this.setState({
                        loading:false
                    })
                    this.props.history.push('/')
                }).catch((err) => {
                    console.log(err)
                    this.setState({
                        loading:false
                    })
                })
            }).catch((err) => {
                console.log(err)
                this.setState({
                    loading:false
                })
            })

        }else{
            toast.dark('OTP do not match.', {
                position: "bottom-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
    }
    clickEvent = (first, last, prv) => {
        if (last != 'none') {
            if (first.target.value) {
                document.getElementById(last).focus();
            }
        }
        if (prv != 'none') {
            if (!first.target.value) {
                document.getElementById(prv).focus();
            }
        }

    }
    otpSetFun = (e, id) => {
        if (id == 'ist') {
            this.setState({
                ist: e.target.value
            })
        }
        if (id == 'sec') {
            this.setState({ sec: e.target.value })
        }
        if (id == 'third') {
            this.setState({ third: e.target.value })
        }
        if (id == 'fourth') {
            this.setState({ fourth: e.target.value })
        }
        if (id == 'fifth') {
            this.setState({ fifth: e.target.value })
        }
        if (id == 'sixth') {
            this.setState({ sixth: e.target.value })
        }
    }
    render() {
        return (
            <main id="content" role="main">
                <div class="bg-img-hero" style={{ backgroundImage: "url(/img/banner/22a5a7b9dbc0e29ccef006dea5981367.png)" }}>
                    <div class="container">
                        <div class="flex-content-center max-width-620-lg flex-column mx-auto text-center min-height-564 ">
                            <div class="loginDiv">
                                <img src="/logo/pawan bullion-01 (1).png" class="loginImage" />
                                {
                                    this.state.otpSendStatus == false?
                                        <div class="extraDivForLogin">
                                            <input type="text" class="form-control inputCustom" placeholder="Name" value={this.state.name} style={{ borderRadius: "5px" }}
                                                onChange={e => this.setState({ name: e.target.value })}
                                            />
                                            <input type="phone" class="form-control inputCustom" placeholder="Phone Number" value={this.state.phone} style={{ borderRadius: "5px", margin: '15px 0' }}
                                                onChange={e => this.setState({ phone: e.target.value })}
                                            />
                                            <input type="email" class="form-control inputCustom" placeholder="Email ID" value={this.state.email} style={{ borderRadius: "5px" }}
                                                onChange={e => this.setState({ email: e.target.value })}
                                            />

                                        </div>
                                        :
                                        <div class="extraDivForLogin">
                                            <span style={{ color: "white", fontSize: "15px", color: "#a7a7a5", padding: "10px" }}>OTP successfully sent to your mobile number <br />
                                                <span style={{ fontSize: "17px", fontWeight: "700", color: "#d7d7d7", paddingLeft: "8px" }}>+91 {this.state.phone}<span className="editSpan" onClick={this.handleEdit}>Edit</span></span>
                                            </span>
                                            <div className="userInput" style={{ marginTop: "15px" }}>
                                                <input className="abc" type="text" id='ist' maxLength="1" onKeyUp={e => this.clickEvent(e, 'sec', 'none')} onChange={e => this.otpSetFun(e, 'ist')} />
                                                <input className="abc" type="text" id="sec" maxLength="1" onKeyUp={e => this.clickEvent(e, 'third', 'ist')} onChange={e => this.otpSetFun(e, 'sec')} />
                                                <input className="abc" type="text" id="third" maxLength="1" onKeyUp={e => this.clickEvent(e, 'fourth', 'sec')} onChange={e => this.otpSetFun(e, 'third')} />
                                                <input className="abc" type="text" id="fourth" maxLength="1" onKeyUp={e => this.clickEvent(e, 'none', 'third')} onChange={e => this.otpSetFun(e, 'fourth')} />
                                                {/* <input className="abc" type="text" id="fifth" maxLength="1" onKeyUp={e => this.clickEvent(e, 'sixth', 'fourth')} onChange={e => this.otpSetFun(e, 'fifth')} />
                                                <input className="abc" type="text" id="sixth" maxLength="1" onKeyUp={e => this.clickEvent(e, 'none', 'fifth')} onChange={e => this.otpSetFun(e, 'sixth')} />
                                          */}
                                            </div>
                                            <span className="editSFd">Didn't receive an OTP? <br /><span className="editSpan1" onClick={()=>this.sendOTP(1)}>Resend OTP</span></span>

                                        </div>
                                }
                                {
                                    this.state.otpSendStatus == false ?
                                        <div class="extraDivForLogin">

                                            <button type="button" class="newBtnSignIn" onClick={this.handleClick} disabled={this.state.loading}>{this.state.loading?<>Loading...</>:<>Continue &nbsp;&nbsp;<BsChevronRight style={{ margin: 0 }} /></>}</button>

                                        </div>
                                        :
                                        <div class="extraDivForLogin">

                                            <button type="button" class="newBtnSignIn" onClick={this.handleLogin} disabled={this.state.loading}>{this.state.loading?<>Loading...</>:<>Continue &nbsp;&nbsp;<BsChevronRight style={{ margin: 0 }} /></>}</button>

                                        </div>
                                }
                                <span className="editSFd">Already have an account? <Link to="/login" className="editSpan1">Please Login</Link></span>


                            </div>
                        </div>
                    </div>
                </div>
                <ToastContainer />
            </main>

        )
    }
}

const mapStateToProps=(state)=>{
	// console.log(state.profile);
	

	return{
	  profile_details: state.profile,
	}
  }

  const mapDispatchToProps=(dispatch)=>{
    return{
        Profile_action:()=>{ dispatch(Show_name())},
    
    }
}

  
export default connect(mapStateToProps,mapDispatchToProps) (Register);


